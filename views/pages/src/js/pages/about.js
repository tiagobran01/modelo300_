class AboutPage extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "Sobre";
  }

  // CANVAS
  // ----------------------------------------

  /**
   * Generates the background for the cases section.
   */
  cv_casesBackground() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('cases');

    /** Setup canvas for animation. */
    let canvas = section.find('.background .canvas').get(0);
    let bg = new about_casesBackground({el: canvas});

    /** Show the background after 1 second. */
    setTimeout(() => section.find('.background').addClass('show'), 1000);
  }

  /**
   * Generates the background for the footer section.
   */
  cv_footerBackground() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('footer');

    /** Setup canvas for animation. */
    let canvas = section.find('.background canvas').get(0);
    let bg = new about_footerBackground();
    bg.canvas = canvas;
    bg.setup();

    /** Activate when scrolling past the section. */

    /** @on_load */
    let scroll_position = $(window).scrollTop() + $(window).height();
    if (scroll_position > section.offset().top) {

      /** @animate */
      if (!section.is('.animated')) setInterval(() => bg.draw(), 1000/60);
      section.addClass('animated');
    }

    /** @on_scroll */
    $(document).on('scroll', () => {

      /** Get the current scroll position. */
      scroll_position = $(window).scrollTop() + $(window).height();

      /** Check if the current scroll position is higher than the secion position. */
      if (scroll_position > section.offset().top) {

        /** @animate */
        if (!section.is('.animated')) setInterval(() => bg.draw(), 1000/60);
        section.addClass('animated');
      }
    })
  }

  // MAIN SETUPS
  // ----------------------------------------

  /** @setup: Hero. */
  setup__hero() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('hero');
    let video = section.find('.background video');

    /** Apply the source to the video. */
    let video_src = video.data('src');
    video.attr('src', video_src).removeAttr('data-src');

    /** Display the video when it has finished loading. */
    video.on('loadeddata', function() {
      $(this).parents('.background').addClass('show');
      video.get(0).play();
    })
  }

  /** @setup: Services. */
  setup__services() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('services');
    let video = section.find('.background video');

    /** Apply the source to the video. */
    let video_src = video.data('src');
    video.attr('src', video_src).removeAttr('data-src');

    /** Display the video when it has finished loading. */
    video.on('loadeddata', function() {
      $(this).parents('.background').addClass('show');
      video.get(0).play();
    })
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {
    let $inst = this;

    /** @setup: Canvas effects. */
    $inst.cv_casesBackground();
    $inst.cv_footerBackground();

    /** Main setups. */
    $inst.setup__hero();
    $inst.setup__services();
  }
}
