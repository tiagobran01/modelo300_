<?php
public ContaBancaria() implements IConta(){
    protected static int $UltimaConta=0;
    protected int $numero;
    
    public function ContaBancaria(){
        $UltimaConta++;
        $this.$numero=$UltimaConta;
    }

    public function double  getSaldo(){
        return $this->saldo;
    }

    public function int getNumeroConta(){
        return $this->numero;
    }

    public function abstract sacar(double $valor);

    public function depositar(double $valor){
        $this->saldo=$this->saldo+$valor;
    }

    public function String ToString(){
        return "numero da conta: " .$this->numero . "Saldo" . $this->saldo;
    }
} 
?>