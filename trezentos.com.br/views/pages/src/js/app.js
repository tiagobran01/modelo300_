
// PAGE CONTROLLER
// ----------------------------------------

/** Instantiate the page controller. */
const pageCtrl = new PageController();

/** Check for current page controller. */
if (typeof eval(window.page_ctrl) === 'function') {

  /**
   * A controller was declared for this page.
   * Instantiate it.
   */
  const Page = new window.page_ctrl;

  /** @init */
  $(document).ready(() => {
    log('Page {'+Page.pageTitle+'} is initializing...', 'gray');
    Page.setup__gravity()
    Page.init()
  })
} else {

  /**
   * A controller was not declared for this page.
   * Log notice.
   */
  log("No controller declared for this page.", "orange", "notice");
}

// GLOBAL ELEMENTS
// ----------------------------------------

/**
 * Setup sticky header on scroll.
 */
$(document).on('scroll', () => {

  /** Get the current scroll position. */
  let scroll_position = $(window).scrollTop();

  /** Check if the current scroll position is higher than 150. */
  if (scroll_position > 150) {
    if (typeof window.scroll_position !== 'undefined') {
      if (scroll_position < window.scroll_position) {
        $('#main-header').addClass('sticky show-actions');
      } else {
        $('#main-header').removeClass('show-actions').addClass('sticky');
      }
    } else {
      $('#main-header').removeClass('show-actions').addClass('sticky');
    }
  } else {
    $('#main-header').removeClass('sticky show-actions');
  }

  /** Update scroll position. */
  window.scroll_position = scroll_position;
});

/**
 * Setup sticky header on load.
 */
(function() {

  /** Get the current scroll position. */
  let scroll_position = $(window).scrollTop();

  /** Check if the current scroll position is higher than 150. */
  if (scroll_position > 150) {
    $('#main-header').addClass('sticky');
  }

  /** Update scroll position. */
  window.scroll_position = scroll_position;
}());

/**
 * Mobile functions.
 */
if ($(window).width() < 1024) {

  /**
   * Animate the loader.
   */
  (function() {
    $(document).ready(function() {
      $('main.page').addClass('animate');
    })
  }());

  /**
   * Detect browser
   */
  (function() {

    /** Get main elements. */
    let modal = $('body').find('#main-menu');

    /** Conditional google chrome. */
    modal.each(function() {
      if (navigator.appVersion.indexOf("Chrome/") != -1) {
        $(this).addClass('only-chrome');
      }
      if (navigator.appVersion.indexOf("Crios/") != -1) {
        $(this).addClass('only-chrome');
      }
      if (navigator.appVersion.indexOf("Version/") != -1) {
        $(this).addClass('only-safari');
      }
      if (navigator.platform === 'MacIntel') {
        $(this).removeClass('only-safari only-chrome');
      }
    })
  }());
}

/**
 * Setup sticky header.
 */
(function() {

  /** Get main variables on load. */
  let offset = $('#app').scrollTop();

  /** Check for offset on load. */
  if (offset > 40) $('#main-header').addClass('sticky');

  /** Check for offset on scroll. */
  $('#app').on('scroll', () => {

    /** Check current offset. */
    offset = $('#app').scrollTop();
    if (offset > 40) {
      $('#main-header').addClass('sticky');
    } else {
      $('#main-header').removeClass('sticky');
    }
  });
}());

// /**
//  * Smooth scrolling on load.
//  */
// $(document).ready(() => $("body").niceScroll());

/**
 * Animate the loader.
 */
(function() {
  $("#loading-screen").addClass('animate');
  setTimeout(() => {
    $(document).ready(() => {
      $("#loading-screen").addClass('fade')
      setTimeout(() => $(window).trigger('entered'), 500)
    });
  }, 700);
}());

/**
 * Animate loader entrance.
 */
$('body').on('click', 'a', function(e) {
  let href = $(this).attr('href');
  let target = $(this).attr('target') || false;

  if (href.indexOf(window.siteurl) !== -1 && target !== '_blank') {
    e.preventDefault();

    /** Display the loader. */
    $('#loading-screen').addClass('cover');
    setTimeout(() => window.location.href = href, 700);
  }
})
