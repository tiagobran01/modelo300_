class HomePage extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "Início";

    /** Main variables. */
    this.slide = {};
  }

  // MAIN SETUPS
  // ----------------------------------------

  /**
   * @setup: Slide.
   */
  setup__slide() {
    let $inst = this;
    log('setup__slide()', 'cyan', 'init');

    /** Get main elements. */
    let section = $inst.section('slide');
    let slide_items = section.find('.slide-items');
    let slide_dots = section.find('.slide-navigation .dots ul');
    let slide_info = section.find('.slide-navigation .info');

    /** Set initial values for the slide info. */
    slide_info.find('.current').text('01');
    slide_info.find('.total').text(('0'+slide_items.find('.item').length).slice(-2));

    /** Append slide dots. */
    for (let x = 0; x < slide_items.find('.item').length; x++) {

      /** @setup: Item HTML. */
      let item_html = ''+
      '<li class="item'+(x === 0 ? ' active' : '')+'" data-index="'+x+'">'+
        '<button type="button"></button>'+
      '</li>'

      /** @append */
      slide_dots.append(item_html);
    }

    /** Bind click event to the arrows. */
    slide_items.find('.item-arrows .arrow').on('click', function() {
      if ($inst.slide.navigation_active === false) return false;

      /** Get direction. */
      let direction = $(this).data('direction');

      /** Change active slide. */
      $inst.slide__move(direction);
    });

    /** Bind click event to the dots. */
    slide_dots.find('.item button').on('click', function() {
      if ($inst.slide.navigation_active === false) return false;

      /** Get index. */
      let index = $(this).parent().data('index');

      /** Change active slide. */
      $inst.slide__move(index);
    });

    /** @setup: Interval. */
    slide_items.find('.item.active .item-arrows .arrow-next').addClass('animate');
    $inst.slide.interval = setInterval(() => {
      $inst.slide__move('next');
    }, 7000);
  }

  // MAIN ACTIONS
  // ----------------------------------------

  /**
   * Updates the page styles based on the current slide.
   *
   * @param {Object} styles Page styles.
   */
  slide__update_styles(styles) {
    let $inst = this;

    /** Apply the header style. */
    $('#main-header').attr('data-style', styles.header);

    /** Apply the wrapper style. */
    $inst.section('slide').children('.slide').attr('data-style', styles.wrapper);
  }

  /**
   * Changes the active slide.
   *
   * @param {String} direction Direction to move the slider to.
   */
  slide__move(direction) {
    let $inst = this;

    /** Deactivate navigation. */
    $inst.slide.navigation_active = false;

    /** Clear the interval. */
    clearInterval($inst.slide.interval);

    /** Get main elements. */
    let section = $inst.section('slide');
    let slide_items = section.find('.slide-items');
    let slide_nav = section.find('.slide-navigation');
    let slide_dots = slide_nav.find('.dots ul');
    let slide_info = slide_nav.find('.info');

    /** Get active slide. */
    let active_slide = slide_items.find('.item.active');
    let active_slide_index = active_slide.data('index');

    /** Get next slide. */
    let next_slide_index;
    if (direction === 'next') {
      next_slide_index = active_slide_index + 1;
    } else if (direction === 'prev') {
      next_slide_index = active_slide_index - 1;
    } else {
      next_slide_index = direction;
    }
    let next_slide = slide_items.find('.item[data-index="'+next_slide_index+'"]');

    /** Check for next slide. */
    if (next_slide.length === 0) {

      /**
       * Next slide not found.
       * Reset the slide index.
       */
      next_slide_index = (direction === 'next') ? 0 : slide_items.find('.item').length;
      next_slide = slide_items.find('.item[data-index="'+next_slide_index+'"]');
    }

    /** Change the active slide. */
    active_slide.removeClass('active').find('.item-arrows .arrow').removeClass('animate');
    slide_dots.find('.item').removeClass('active');
    slide_dots.find('.item[data-index="'+next_slide_index+'"]').addClass('active');
    slide_info.find('.current').text(('0'+(next_slide_index+1)).slice(-2));
    setTimeout(() => {
      active_slide.removeClass('animate');
      next_slide.addClass('active animate');
      setTimeout(() => {
        next_slide.find('.item-arrows .arrow-next').addClass('animate');
      });
    }, 1000);

    /** @get: Styles. */
    let styles = {
      header: next_slide.data('header-style'),
      wrapper: next_slide.data('style')
    }

    /** @update: Styles. */
    $inst.slide__update_styles(styles);

    /** Reset the interval. */
    setTimeout(() => {
      $inst.slide.interval = setInterval(() => {
        $inst.slide__move('next');
      }, 7000);
    }, 1000);

    /** Activate navigation. */
    setTimeout(() => $inst.slide.navigation_active = true, 2000);
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {
    let $inst = this;

    /** Main setups. */
    $inst.setup__slide();
  }
}
