
/**
 * Displays a progress screen.
 *
 * @param {string} message Message to be displayed while the progress is still going.
 */
function progress_screen(message) {

  /** @setup [Screen] */
  let screen_html = `
    <div class="screen-progress">
      <div class="message-wrapper">
        <div class="screen-message">${message}</div>
      </div>
    </div>
  `;
  let screen = $(screen_html);

  /** Append screen to main body. */
  $("body").append(screen);

  /** Display screen. */
  setTimeout(() => screen.addClass("open"), 300);

  /** Bind event to close. */
  screen.on("close", function(e, data) {
    if (data) {
      if (typeof data === "object") {
        if (data.status) screen.addClass(`status-${data.status}`)
        screen.find(".screen-message").html(data.message);
        setTimeout(() => {
          screen.removeClass("open");
          setTimeout(() => screen.remove(), 1000);
        }, data.timeout);
      } else {
        screen.find(".screen-message").html(data);
        setTimeout(() => {
          screen.removeClass("open");
          setTimeout(() => screen.remove(), 1000);
        }, 1000);
      }
    } else {
      screen.removeClass("open");
      setTimeout(() => screen.remove(), 1000);
    }
  });

  /** @return */
  return screen;
}

/**
 * Displays a notification on the screen.
 *
 * @param {String} message Message to be displayed.
 * @param {String} color   Alert color.
 * @param {Number} timeout Timeout before the alert is removed.
 */
function notify(message, color="azure", timeout=5000) {

  /** @setup: Alert HTML. */
  let alert_html = '<div id="alert" class="alert alert-'+color+'">'+message+'</div>';

  /** Check for other alert present on this page/ */
  if ($('#alert').length !== 0) {
    $('#alert').removeClass('show');
    setTimeout(() => {
      $('#alert').remove();

      /** @append */
      $('body').append(alert_html);
      let new_alert = $('#alert');

      /** Display, then remove. */
      setTimeout(() => {
        new_alert.addClass('show');
        setTimeout(() => {
          new_alert.removeClass('show');
          setTimeout(() => new_alert.remove(), 300);
        }, timeout);
      }, 100);
    }, 300);
  } else {

    /**
     * There is no other alert present.
     * Display the new one.
     */

    /** @append */
    $('body').append(alert_html);
    let new_alert = $('#alert');

    /** Display, then remove. */
    setTimeout(() => {
      new_alert.addClass('show');
      setTimeout(() => {
        new_alert.removeClass('show');
        setTimeout(() => new_alert.remove(), 300);
      }, timeout);
    }, 100);
  }
}
