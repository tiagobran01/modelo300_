class Yuri {

  /** Main information for this page. */
  constructor(page) {

    /** Save the chat box and its information. */
    this.chat_stage = page.chat.stage;
    this.chat_info = page.chat.info;
    this.chat_box = page.chat.box;
  }

  // BRAIN
  // ----------------------------------------

  /**
   * Processes the received message and returns
   * what Yuri understood from it.
   *
   * @param {String} data Received message.
   */
  process(data) {
    let $inst = this;

    /** Prepare validation data. */
    data = data.toLowerCase();

    /** List check types. */
    let check_types = [
      'word_count',
      'introduction',
      'name',
      'email',
      'phone',
      'swearing'
    ];

    /** List check functions. */
    let check = {

      /** ([Word Count]) */
      word_count: () => new Promise(resolve => {

        /** @resolve */
        resolve(data.split(' ').length);
      }),

      /** ([Introduction]) */
      introduction: () => new Promise(resolve => {

        /** Check for nice introduction. */
        if (data.match(/olá|oi|ola/g) !== null) {
          resolve('nice');
        } else {
          resolve('normal');
        }
      }),

      /** ([Name]) */
      name: () => new Promise(resolve => {

        /** Declare main variables. */
        let has_prefix = false;
        let given_name = '';

        /**
         * Check for a prefix.
         */
        let prefix_check = data.match(/nome é|nome e/g);
        if (prefix_check !== null) has_prefix = true;

        /** ?= Has prefix. */
        if (has_prefix) {

          /** Get name from the message. */
          if (prefix_check.indexOf('nome é') !== -1) {
            given_name = data.split('nome é')[1].replace('.', '').trim();
          } else if (prefix_check.indexOf('nome e') !== -1) {
            given_name = data.split('nome e')[1].replace('.', '').trim();
          }

          /** Check if the name matches a format. */
          if (/^[A-Za-z ]+$/.test(given_name) === false) {

            /**
             * It's definitely not a name.
             * Do not save it.
             */

            /** @resolve [Invalid] */
            resolve({
              status: 'invalid',
              info: 'not_a_name'
            });
          } else {

            /**
             * It looks like a name.
             * Format it and save it.
             */

            /** Get only the first two names. */
            let split_name = given_name.split(' ').splice(0, 2);

            /** Format given name. */
            given_name = '';
            for (let part of split_name) {
              given_name += part[0].toUpperCase() + part.slice(1) + ' ';
            }
            given_name = given_name.trim();

            /** @resolve */
            resolve(given_name);
          }
        } else {

          /**
           * No prefix found.
           */
          if (data.split(' ').length > 3) {

            /**
             * The message is too long.
             * Tell the user to be more specific.
             */
            resolve({
              status: 'invalid',
              info: 'too_long'
            });
          } else {

            /**
             * The message is long enough.
             */
            if (/^[A-Za-z ]+$/.test(data) === false) {

              /**
               * It's definitely not a name.
               * Do not save it.
               */

              /** @resolve [Invalid] */
              resolve({
                status: 'invalid',
                info: 'not_a_name'
              });
            } else {

              /**
               * It looks like a name.
               * Format it and save it.
               */

              /** Get only the first two names. */
              let split_name = data.split(' ').splice(0, 2);

              /** Format given name. */
              for (let part of split_name) {
                given_name += part[0].toUpperCase() + part.slice(1) + ' ';
              }
              given_name = given_name.trim();

              /** @resolve */
              resolve(given_name);
            }
          }
        }
      }),

      /** ([E-mail]) */
      email: () => new Promise(resolve => {

        /** @declare: E-mail regex. */
        let email_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        /** Attempt to find any @ among the data. */
        let message = data.split(' ');
        let at_found = false;
        for (let x = 0; x < message.length; x++) {
          if (message[x].indexOf('@') !== -1) {
            at_found = x;
          }
        }

        /** Check for found @. */
        if (at_found !== false) {

          /** Get the given email. */
          let given_email = message[at_found];

          /** Format e-mail. */
          given_email = given_email.replace(/[\,]/, '');

          /** Check the email validity. */
          if (email_regex.test(given_email) === false) {

            /**
             * The given e-mail is not valid.
             * Do not save it.
             */

            /** @resolve [Invalid] */
            resolve({
              status: 'invalid',
              info: 'not_an_email'
            });
          } else {

            /**
             * The given e-mail is valid.
             * Save it.
             */

            /** @resolve */
            resolve(given_email);
          }
        } else {

          /**
           * No e-mail was given.
           * Do not save it.
           */

          /** @resolve [Not-Found] */
          resolve({
            status: 'invalid',
            info: 'not_found'
          });
        }
      }),

      /** ([Phone]) */
      phone: () => new Promise(resolve => {

        /** Check data length. */
        if (data.length <= 5) {

          /**
           * No phone number given.
           * Do not save it.
           */

          /** @resolve [Invalid] */
          resolve({
            status: 'invalid',
            info: 'phone_not_given'
          });
        } else if (data.length < 14) {

          /**
           * Invalid phone number.
           * Do not save it.
           */

          /** @resolve [Invalid] */
          resolve({
            status: 'invalid',
            info: 'missing_characters'
          });
        } else {

          /**
           * A valid phone number was given.
           * Save it.
           */

          /** @resolve */
          resolve(data);
        }
      }),

      /** ([Swearing]) */
      swearing: () => new Promise(resolve => {

        /** List swearing words. */
        $.ajax({
          url: window.siteurl + '/views/pages/src/assets/files/swearing-words.json',
          method: 'GET',
          dataType: 'text',
          success: response => {
            let list = JSON.parse(response);
            list = list.list;

            /** Find any of the words in the list. */
            let count = 0;
            for (let word of list) {
              if (data.split(' ').indexOf(word) !== -1) count++
            }

            /** @resolve */
            resolve(count);
          },
          error: error => console.error('Error:', error)
        })
      })
    }

    /** Setup the results. */
    let results = {}

    /** Loop through each check type and save the result. */
    let promise = new Promise(resolve => {

      /** @step */
      function step(x) {
        check[check_types[x]]().then(result => {

          /** Save result. */
          results[check_types[x]] = result;

          /** @finish */
          if ((x + 1) === check_types.length) {
            resolve(results);
          } else {
            x++;
            step(x);
          }
        })
      } let x = 0; step(x);
    });

    /** @return */
    return promise;
  }

  /**
   * Thinks of an answer for the user.
   *
   * @param {Object} data Processed information with key values.
   */
  think(data) {
    let $inst = this;

    /** List answers. */
    let answers_list = {
      general: {

        /**
         * Simple swearing.
         * Advert.
         */
        simple_swearing: [
        ['Opa...',
         'Vamos manter a seriedade na conversa, por favor. 😅']],

        /**
         * Medium swearing.
         * Advert.
         */
        medium_swearing: [
        ['Err...',
         'Quer dizer que alguém é boca suja, então?',
         'Vou fingir que você não foi por este caminho sombrio da conversa.',
         '😶']],

        /**
         * Heavy swearing.
         * Advert.
         */
        heavy_swearing: [
        ['Ei... Não precisa disso.',
         'Somos todos amigos aqui.',
         '#paz ✌️']]
      },

      stage_1: {

        /**
         * Simple try again.
         */
        try_again: [
        ['Vamos tentar de novo? 😅',
         'Qual é o seu nome?']],

        /**
         * Message too long.
         * Try again.
         */
        too_long: [
        ['Vamos tentar de novo?',
        'Me diga o seu nome, <em>em poucas palavras</em>.']],

        /**
         * Invalid name given.
         * Try again.
         */
        invalid_name: [
        ['Acho que você está brincando comigo. 🤔'],
        ['Vamos tentar outra vez.',
         'Qual é o seu nome? ☺️']],

        /**
         * Strange name given.
         * Keep going.
         */
        strange_name: [
        ['Que nome diferente você tem, não!?',
         'Mas quem sou eu para julgar?',
         'Eu nem existo! 🤷🏻‍'],
        ['É um prazer falar com você, {{name}}!',
         'Para que a gente possa te enviar um orçamento, por favor, me passe o seu e-mail.']],

        /**
         * [4]
         * Name given.
         * Everything okay.
         */
        ok: [
        ['É um prazer falar com você, {{name}}!'],
        ['Para que a gente possa te enviar um orçamento, por favor, me passe o seu e-mail.']]
      },

      stage_2: {

        /**
         * E-mail not found.
         */
        email_not_found: [
        ['Isso não fez muito sentido.',
         'Vou precisar do seu <em><strong><u>e-mail</u></strong></em> para enviar um orçamento. ✉️'],
        ['<em>Ps.: Um e-mail é um endereço eletrônico.</em>']],

        /**
         * Invalid e-mail address.
         */
        invalid_email: [
        ['Hmmm... Esse e-mail não me parece válido. 🤔']],

        /**
         * Correct e-mail given.
         */
        ok: [
        ['Ótimo. E qual é o seu telefone?',
        '(Prometo não passar trote 😂).']]
      },

      stage_3: {

        /**
         * Phone not found.
         */
        phone_not_given: [
        ['Ei, está tendo problemas para escrever o número? 📞',
         'Tente novamente, por favor.']],

        /**
         * Invalid phone number.
         */
        missing_characters: [
        ['Este número de telefone não me parece válido.',
         'Faltam alguns dígitos. 😅']],

        /**
         * Valid phone number given.
         */
        ok: [
        ['Excelente!',
         'Um momento, por favor...']]
      }
    }

    /** List conversation stages. */
    let stage = {

      /** Stage 1. */
      '1': () => new Promise(resolve => {

        /** Check if a name was given. */
        if (typeof data.name === 'object') {
          if (data.name.info === 'too_long') {
            resolve({
              stage_1: 'too_long'
            });
          } else if (data.name.info === 'not_a_name') {
            resolve({
              stage_1: 'invalid_name'
            });
          }
        } else {
          if (data.swearing > 1) {
            resolve({
              stage_1: 'try_again'
            });
          } else {
            if (data.swearing === 1) {
              resolve({
                stage_1: 'strange_name'
              })
            } else {
              resolve({
                stage_1: 'ok'
              });
            }

            /** Save given name. */
            $inst.chat_info.name = data.name;

            /** Change the chat stage. */
            $inst.chat_stage = 2;
          }
        }
      }),

      /** Stage 2. */
      '2': () => new Promise(resolve => {

        /** Check if an e-mail was given. */
        if (typeof data.email === 'object') {
          if (data.email.info === 'not_found') {
            resolve({
              stage_2: 'email_not_found'
            });
          } else if (data.email.info === 'not_an_email') {
            resolve({
              stage_2: 'invalid_email'
            });
          }
        } else {
          resolve({
            stage_2: 'ok'
          });

          /** Save given email. */
          $inst.chat_info.email = data.email;

          /** Change the chat stage. */
          $inst.chat_stage = 3;
        }
      }),

      /** Stage 3. */
      '3': () => new Promise(resolve => {

        /** Check if a phone number was given. */
        if (typeof data.phone === 'object') {
          if (data.phone.info === 'phone_not_given') {
            resolve({
              stage_3: 'phone_not_given'
            });
          } else if (data.phone.info === 'missing_characters') {
            resolve({
              stage_3: 'missing_characters'
            });
          }
        } else {
          resolve({
            stage_3: 'ok'
          });

          /** Save given email. */
          $inst.chat_info.phone = data.phone;

          /** Change the chat stage. */
          $inst.chat_stage = 4;
        }
      })
    }

    /** Start checking. */
    let promise = new Promise(resolve => {

      /** Declare final list. */
      let final_answers = [];

      /** Run general check. */
      if (data.swearing !== 0) {
        if (data.swearing === 1 && $inst.chat_stage !== 1) {
          final_answers = final_answers.concat(answers_list.general.simple_swearing);
        } else if (data.swearing === 2) {
          final_answers = final_answers.concat(answers_list.general.medium_swearing);
        } else if (data.swearing >= 3) {
          final_answers = final_answers.concat(answers_list.general.heavy_swearing);
        }
      }

      /** Run stage check. */
      stage[$inst.chat_stage]().then(answers => {

        /** Loop through answers and populate the final list. */
        for (let category in answers) {
          final_answers = final_answers.concat(answers_list[category][answers[category]]);
        }

        /** @resolve */
        resolve(final_answers);
      });
    });

    /** @return */
    return promise;
  }

  // ACTIONS
  // ----------------------------------------

  /**
   * Say something.
   *
   * @param {String} messages Messages to send.
   */
  say(...messages) {
    let $inst = this;

    /** @setup: Messages. */
    for (let x = 0; x < messages.length; x++) {
      if (typeof messages[x] !== 'object') {
        messages[x] = '<li class="message">'+messages[x]+'</li>';
      }
    }

    /** Send messages. */
    return new Promise(resolve => {
      $inst.send(messages).then(() => resolve());
    })
  }

  /**
   * Sends a list of messages to the chat box.
   *
   * @param  {Array}   messages List of messages to send.
   * @return {Promise}          Resolved when finished.
   */
  send(messages) {
    let $inst = this;

    /** Start typing. */
    $inst.chat_box.addClass('typing');
    $inst.chat_box.animate({
      scrollTop: $inst.chat_box.get(0).scrollHeight
    }, 500);

    /** Format the first message. */
    let first_message = messages[0];
    if (typeof first_message !== 'object') {

      /** Replace any present keywords. */
      first_message = messages[0].replace(/{{\w+}}/g, key => {
        key = key.replace('{{', '').replace('}}', '');

        /** Get value to format. */
        let value = $inst.chat_info[key];

        /** Check if it's a name. */
        if (key === 'name' && value !== false) value = value.split(' ')[0];

        /** @return */
        return value;
      });
    } else {

      /**
       * It's a special message.
       * Build it.
       */

      /** Get message attributes. */
      let message_attr = first_message;

      /** @build */
      switch (message_attr.type) {
        case "button":
          first_message = `<li class="button"><a href="${message_attr.href}">${message_attr.text}</a></li>`;
          break;
      }
    }

    /** Setup main template including the first message. */
    let template = ''+
    '<div class="header">'+
      '<div class="name">Yuri</div>'+
    '</div>'+
    '<div class="list">'+
      '<ul>'+first_message+'</ul>'+
    '</div>';

    /** Create message group. */
    let message_group = document.createElement('div');
    message_group.innerHTML = template;
    $(message_group).addClass('message-group message-group-yuri');

    /** Insert the message group into the chat box. */
    let promise = new Promise(resolve => {
      setTimeout(() => {
        $inst.chat_box.append(message_group);

        /** Scroll to bottom. */
        $inst.chat_box.get(0).scrollTop = $inst.chat_box.get(0).scrollHeight;

        /** Append other messages. */
        if (messages.length > 1) {

          /** Remove the first message from the list. */
          messages = messages.slice(1);

          /** Loop through other messages. */
          for (let x = 0; x < messages.length; x++) {
            let message = messages[x];

            /** Replace any present keywords. */
            if (typeof message !== 'object') {
              message = message.replace(/{{\w+}}/g, key => {
                return $inst.chat_info[key];
              });
            } else {

              /**
               * It's a special message.
               * Build it.
               */

              /** Get message attributes. */
              let message_attr = message;

              /** @build */
              switch (message_attr.type) {
                case "button":
                  message = '<li class="button"><a href="'+message_attr.href+'">'+message_attr.text+'</a></li>';
                  break;
              }
            }

            /** @prepare: Delay between messages. */
            let delay = 1000 * (x + 1);

            /** Append message. */
            setTimeout(() => {
              $(message_group).find('.list ul').append(message);
              $inst.chat_box.get(0).scrollTop = $inst.chat_box.get(0).scrollHeight;

              /** Stop typing. */
              if ((x + 1) === messages.length) {
                setTimeout(() => {
                  $inst.chat_box.removeClass('typing');

                  /** @finish */
                  resolve();
                }, 300);
              }
            }, delay);
          }
        } else {

          /**
           * No other messages found.
           * Stop typing.
           */
          setTimeout(() => {
            $inst.chat_box.removeClass('typing');

            /** @finish */
            resolve();
          }, 300);
        }
      }, 1000);
    });

    /** @return */
    return promise;
  }

  /**
   * Says goodbye to the user and ends the conversation.
   */
  say_goodbye() {
    let $inst = this;

    /** @prepare: Script. */
    let script;

    /** Check mobile. */
    if ($(window).width() <= 1024) {
      script = [
      ['Prontinho! Já guardei as suas informações.',
       'Em breve entraremos em contato.',
       'Até mais! 😉👊']]
    } else {
      script = [
      ['Prontinho! Já guardei as suas informações.',
       'Em breve entraremos em contato.',
       'Bom, aproveita que você está aqui e clica no botão abaixo pra conhecer alguns de nossos projetos. Até mais! 😉👊',
       {type: 'button', text: 'Clique aqui e conheça os nossos projetos!', href: `${window.siteurl}/projetos`}]];
    }

    /** @step */
    let promise = new Promise(resolve => {
      let x = 0;
      function step(x) {
        let delay = 1000 * (x + 1);

        /** Send reply. */
        setTimeout(() => $inst.say(...script[x]).then(() => {

          /** Decide what to do next. */
          if ((x + 1) < script.length) {
            x++;
            step(x);
          } else {
            setTimeout(() => {
              $inst.chat_box.append('<div class="alert"><span><strong>Yuri</strong> saiu da conversa.</span></div>');
              $inst.chat_box.get(0).scrollTop = $inst.chat_box.get(0).scrollHeight;
              resolve();
            }, 500);
          }
        }), delay);
      } step(x);
    });

    /** @return */
    return promise;
  }

  /**
   * Receives a message from the user.
   *
   * @param {String} message Message received.
   * @param {Numbr}  stage   Current stage in the conversation.
   */
  receive(message) {
    let $inst = this;
    // log('Message received.', 'blue', 'yuri');

    /** Setup promise. */
    let promise = new Promise(resolve => {

      /** Process the received message. */
      // log('Processing message...', 'cyan', 'yuri');
      $inst.process(message).then(results => {
        // console.log('Results:', results);

        /** Think of an answer. */
        // log('Thinking...', 'cyan', 'yuri');
        $inst.think(results, $inst.chat_stage).then(answers => {
          // console.log('Answers found:', answers);
          // log(['Current stage:', $inst.chat_stage], 'orange', 'chat');

          /** @step */
          let x = 0;
          function step(x) {
            let delay = 1000 * x;

            /** Send reply. */
            setTimeout(() => $inst.say(...answers[x]).then(() => {

              /** Decide what to do next. */
              if ((x + 1) === answers.length) {
                resolve({
                  info: $inst.chat_info,
                  stage: $inst.chat_stage
                });
              } else {
                x++;
                step(x);
              }
            }), delay);
          } step(x);
        });
      });
    })

    /** @return */
    return promise;
  }
}
