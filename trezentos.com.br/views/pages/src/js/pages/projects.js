class ProjectsPage extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "Projetos";

    /** Main variables. */
    this.audio = {};
  }

  // UTILITIES
  // ----------------------------------------

  /**
   * Calculates the current scroll percentage.
   */
  get_scroll_percentage() {

    /** Get main elements. */
    let h = document.documentElement;
    let b = document.body;
    let st = 'scrollTop';
    let sh = 'scrollHeight';

    /** Calculate the scroll percentage. */
    let scroll_percentage = (h[st]||b[st]) / ((h[sh]||b[sh]) - h.clientHeight) * 100;
    scroll_percentage = 100 - scroll_percentage;

    /** @return */
    return scroll_percentage;
  }

  // CANVAS
  // ----------------------------------------

  /**
   * Page background.
   */
  cv__background() {
    let $inst = this;

    /** Get main elements. */
    let canvas = $inst.currentPage.find('.page-background canvas');

    /** Setup canvas for animation. */
    let bg = new projects_MainBackground();
    bg.canvas = canvas.get(0);
    bg.prepare();
    bg.init();

    /** @animate */
    setInterval(() => bg.animate(), 1000/60);
  }

  // MAIN SETUPS
  // ----------------------------------------

  /**
   * @setup: Audio.
   */
  setup__audio() {
    let $inst = this;

    /** @get: All audio. */
    let audio = $inst.currentPage.find('[rel="audio"]');

    /** Save each audio to the main controller. */
    audio.each(function() {

      /** Get audio information. */
      let src = $(this).data('src');
      let name = $(this).data('name');

      /** Setup options. */
      let audio_options = {
        src: [src],
        autoplay: false,
        loop: (name === 'background') ? true : false,
        volume: (name === 'background') ? 0 : 1
      }

      /** Create object. */
      let audio = new Howl(audio_options);

      /** @save */
      $inst.audio[name] = audio;
    });
  }

  /**
   * @setup: Parallax effect for the columns.
   */
  setup__parallax() {
    let $inst = this;

    /** Get main elements. */
    let columns = $inst.currentPage.find('.columns .column');

    /** Save initial scroll position. */
    $inst.scrollY = $(window).scrollTop();

    /** Change column position on scroll. */
    $(window).on('scroll', function(e) {

      /** Get scroll position and setup scroll direction. */
      let scroll_position = $(window).scrollTop();
      let scroll_direction = (scroll_position > $inst.scrollY) ? 'down' : 'up';
      $inst.scrollY = scroll_position;

      /** Get document height. */
      let document_height = $('body').outerHeight();

      /** Loop through each column and update its position. */
      columns.each(function() {

        /** Get column index. */
        let column_index = $(this).data('column');

        /** Get current offset. */
        let current_offset = $(this).attr('data-offset');
        let column_offset = (typeof current_offset !== 'undefined') ? Number(current_offset) : 0;

        /** Setup column offset. */
        if (scroll_position < (document_height / 2)) {

          /**
           * The user is still scrolling in the first half of the document.
           * Keep adding numbers to the offset.
           */
          switch (column_index) {
            case 2:
              column_offset = (scroll_direction === 'down') ? column_offset + 1 : column_offset - 1;
              break;
            case 3:
              column_offset = (scroll_direction === 'down') ? column_offset - 1 : column_offset + 1;
              break;
          }
        } else {

          /**
           * The user is scrolling past the first half of the document.
           * Revert the parallax effect.
           */
          switch (column_index) {
            case 2:
              column_offset = (scroll_direction === 'down') ? column_offset - 1 : column_offset + 1;
              break;
            case 3:
              column_offset = (scroll_direction === 'down') ? column_offset + 1 : column_offset - 1;
              break;
          }
        }

        /** Update column position. */
        $(this).css('transform', 'translateY('+column_offset+'px)').attr('data-offset', column_offset);
      })
    })
  }

  /**
   * @setup: Project filters.
   */
  setup__filters_OLD() {
    let $inst = this;

    /** Get main elements. */
    let filters = $inst.currentPage.find('.content-above .right [rel="filters"] .item');
    let projects_list = $inst.currentPage.find('.content-below .columns');

    /** Bind click event to filters. */
    filters.find('button').on('click', function() {

      /** Main variables. */
      let item = $(this).parents('.item');
      let filter = item.data('type');

      /** Get matching projects. */
      let matching_projects = projects_list.find('.project[data-type="'+filter+'"]');
      let not_matching_projects = projects_list.find('.project:not([data-type="'+filter+'"])');

      /** Check if this filter is applied. */
      if (item.is('.active')) {
        $inst.audio__play('neon-effect', 1.4);

        /** Deactivate this filter. */
        item.removeClass('active');

        /** Remove the highlight from matching projects. */
        matching_projects.removeClass('highlighted');

        /** Check if this is the last filter. */
        if (projects_list.find('.project.highlighted').length === 0) {
          $inst.audio__play('neon-effect', 0.35);
          setTimeout(() => $inst.audio['neon-effect'].fade(1, 0, 3000), 500);
          setTimeout(() => $inst.audio['neon-effect'].fade(0, 1, 100), 4000);

          /**
           * There are no other filters applied.
           * Display all the projects.
           */
          projects_list.find('.project').removeClass('disabled');
        } else {
          $inst.audio__play('neon-effect', 1.4);

          /**
           * There are other filters applied.
           * Disable the current filter.
           */
          matching_projects.addClass('animate-off');
          setTimeout(() => matching_projects.removeClass('animate-off').addClass('disabled'), 500);
        }
      } else {

        /** Activate this filter. */
        item.addClass('active');

        /** Check if there are other filters applied. */
        if (projects_list.find('.project.highlighted').length === 0) {
          $inst.audio__play('neon-effect', 0.4);

          /**
           * There are no other filters applied.
           * Disable all projects, except the othes matching this filter.
           */
          not_matching_projects.addClass('animate-off');
          setTimeout(() => not_matching_projects.removeClass('animate-off').addClass('disabled'), 500);

          /** Highlight the projects matching this filter. */
          matching_projects.addClass('highlighted');
        } else {
          $inst.audio__play('neon-effect', 0.7);

          /**
           * There are other filters applied.
           * Apply this filter.
           */
          matching_projects.removeClass('disabled').addClass('highlighted');
        }
      }
    })
  }

  /** @setup: Filters. */
  setup__filters() {
    let $inst = this

    /** @get: Main elements. */
    let filtersList = this.currentPage.find('.content-above .right [rel="filters"]')

    /** Bind click event to the filters. */
    filtersList.find('.item button').on('click', function() {
      if (filtersList.is('.disabled')) return false;
      filtersList.addClass('disabled')

      /** @get: The item. */
      let item = $(this).parents('.item')
      let itemFilter = item.data('type')

      /** @get: Applied filters. */
      let appliedFilters = filtersList.data('appliedFilters') || []
      appliedFilters.reverse()

      /** Change filter status. */
      let filterIndex = appliedFilters.indexOf(itemFilter)

      if (item.is('.active')) {
        if (filterIndex !== -1) appliedFilters.splice(filterIndex, 1);
      } else {
        if (filterIndex === -1) appliedFilters.push(itemFilter);
      }

      item.toggleClass('active')
      filtersList.data('appliedFilters', appliedFilters)

      /** @get: Matching entries. */
      let matchingEntries = []
      for (let filter of appliedFilters.reverse()) {
        $inst.currentPage.find(`.content-below .columns .project[data-type="${filter}"]`).each(function() {
          matchingEntries.push(this)
        })
      }

      /** @get: Non-matching entries. */
      let nonMatchingEntries = []
      $inst.currentPage.find('.content-below .columns .project').each(function() {
        if (matchingEntries.indexOf(this) === -1) nonMatchingEntries.push(this)
      })

      for (let x = 0; x < nonMatchingEntries.length; x++) {
        let y = Math.floor(Math.random() * (x + 1))
        let temp = nonMatchingEntries[x]
        nonMatchingEntries[x] = nonMatchingEntries[y]
        nonMatchingEntries[y] = temp
      }

      /** @get: All entries. */
      let allEntries = matchingEntries.concat(nonMatchingEntries)

      /** Sort entries. */
      allEntries.sort((a, b) => {
        for (let project of matchingEntries) {
          if (project === a) return -1;
          if (project === b) return 1;
        }
        return 0;
      })

      /** Hide all entries. */
      for (let project of allEntries) project.classList.add('fade');

      let timeout = (allEntries.length * 50) + 500;
      setTimeout(() => {
        for (let x = 0; x < allEntries.length; x++) {
          $(allEntries[x]).attr('data-index', x + 1)
        }
      }, timeout)

      /** Reorder and display entries. */
      setTimeout(() => {
        for (let p of matchingEntries) {
          p.classList.remove('disabled')
          p.classList.add('highlighted')
        }

        for (let p of nonMatchingEntries) {
          p.classList.add('disabled')
          p.classList.remove('highlighted')
        }

        if (appliedFilters.length === 0) {
          for (let p of allEntries) {
            p.classList.remove('disabled')
            p.classList.remove('highlighted')
          }
        }

        for (let project of allEntries) {
          $inst.currentPage.find('.content-below .columns').append(project)
          setTimeout(() => project.classList.remove('fade'), 100)
        }

        setTimeout(() => filtersList.removeClass('disabled'), 500)
      }, timeout)

      $('html, body').animate({
        scrollTop: 0
      },{
        duration: 1300,
        easing: $.bez([0.5, 0, 0, 1])
      })
    })
  }

  /**
   * @setup: Scroll animation.
   */
  setup__scroll_animation() {
    let $inst = this;

    /** Get main elements. */
    let scroll_wrapper = $inst.currentPage.find('.scroll[rel="scroll-wrapper"]');

    /** Get current scroll percentage. */
    let scroll_percentage = $inst.get_scroll_percentage();

    /** Set initial scroll percentage. */
    scroll_wrapper.find('.line').animate({
      height: scroll_percentage
    },{
      duration: 300,
      easing: $.bez([0.5, 0, 0, 1])
    });

    /** Turn the button into [scroll-to-top] if the percentage is 0. */
    if (scroll_percentage === 0)
      setTimeout(() => scroll_wrapper.addClass('scroll-to-top'), 300);

    /** Change scroll point on scroll. */
    $(window).on('scroll', function() {

      /** Get scroll point. */
      let scroll_percentage = $inst.get_scroll_percentage();

      /** Change the line length; */
      scroll_wrapper.find('.line').css('height', scroll_percentage);

      /** Turn the button into [scroll-to-top] if the percentage is 0. */
      if (scroll_percentage === 0) {
        scroll_wrapper.addClass('scroll-to-top');
      } else {
        scroll_wrapper.removeClass('scroll-to-top');
      }
    });
  }

  /**
   * @setup: Scroll to top.
   */
  setup__scroll_to_top() {
    let $inst = this;

    /** Get main elements. */
    let scroll_wrapper = $inst.currentPage.find('.scroll[rel="scroll-wrapper"]');

    /** Bind click event to the scroll wrapper. */
    scroll_wrapper.on('click', function() {
      if (!$(this).is('.scroll-to-top')) return false;

      /** Scroll to top. */
      $('body, html').animate({
        scrollTop: 0
      },{
        duration: 1300,
        easing: $.bez([0.5, 0, 0, 1])
      })
    })
  }

  // AUDIO
  // ----------------------------------------

  /**
   * Starts playing an audio.
   *
   * @param {String}  name Audio name.
   * @param {Boolean} fade Wether or not to fade the audio.
   * @param {Number}  seek At what point should the audio be played.
   *                       Default 0
   */
  audio__play(name, seek=0) {
    let $inst = this;
    // log(['Playing', name], 'cyan', 'audio');

    /** Get the audio. */
    let audio = $inst.audio[name];

    /** Start playing. */
    // audio.stop();
    // audio.play();
    // audio.seek(seek);
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {
    let $inst = this;

    /** Main setups. */
    $inst.setup__audio();
    // $inst.setup__parallax();
    $inst.setup__filters();
    $inst.setup__scroll_animation();
    $inst.setup__scroll_to_top();

    /** @setup: Canvas effects. */
    $inst.cv__background();
  }
}
