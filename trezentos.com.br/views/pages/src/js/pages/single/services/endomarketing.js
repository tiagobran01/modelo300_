class EndomarketingSingle extends PageController {
  constructor() {
    super();
    this.pageTitle = "Serviços - Endomarketing";
  }

  // SECTION 5
  // ----------------------------------------

  /** @setup: Slide. */
  setup__sectionFiveSlide() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('5')

    /** @inst: Slick. */
    section.find('.slide .slide-items').slick({
      arrows: false,
      dots: true,
      appendDots: section.find('.slide .slide-navigation'),
      speed: 500,
      cssEasing: 'cubic-bezier(.5, 0, 0, 1)',
      autoplay: true,
      autoplaySpeed: 4000
    })
  }

  // INIT
  // ----------------------------------------

  init() {
    let $inst = this;

    /** Main setups. */
    $inst.setup__sectionFiveSlide()
  }
}
