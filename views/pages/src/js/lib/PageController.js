class PageController {

  /**
   * Initial setups for the controller.
   */
  constructor() {

    /** Current page. */
    this.currentPage = $('main.page');
    this.currentPageKey = window.page_key;
  }

  /**
   * Retrieves a specific section in the current page.
   *
   * @param  {String} key Section key.
   * @return {DOM}        Section element, if found.
   */
  section(key) {

    /** Attempt to find the specified section. */
    let section = this.currentPage.find('.section[key="'+key+'"]');
    return (section.length !== 0) ? section : false;
  }

  // SETUPS
  // ----------------------------------------

  /** @setup: Gravity effect. */
  setup__gravity() {
    $(document).on('mousemove', function(m) {
      $('[data-gravity]').each(function() {

        /** @declare: Main variables. */
        let posX = m.pageX
        let posY = m.pageY
        let el = $(this)
        let maxDist = el.data('gravity') || 120
        let el_middleX = el.offset().left + el.outerWidth() / 2
        let el_middleY = el.offset().top + el.outerHeight() / 2
        let offsetX = -.45 * Math.floor(el_middleX - posX)
        let offsetY = -.45 * Math.floor(el_middleY - posY)

        /** @prepare: Duplicates. */
        let dp_el = el
        let dp_posX = posX
        let dp_posY = posY

        /** Calculate the maximum distance. */
        let calc = Math.floor(Math.sqrt(Math.pow(dp_posX - (dp_el.offset().left + dp_el.outerWidth() / 2), 2) + Math.pow(dp_posY - (dp_el.offset().top + dp_el.outerHeight() / 2), 2)))

        /** @move */
        if (calc < maxDist) {
          TweenMax.to(el, 1, {
            x: offsetX,
            y: offsetY
          })
        } else {
          TweenMax.to(el, 1, {
            x: 0,
            y: 0
          })
        }

        $('span[rel="calc"]').text(calc)
        $('span[rel="maxDist"]').text(maxDist)
      })
    })
  }
}
