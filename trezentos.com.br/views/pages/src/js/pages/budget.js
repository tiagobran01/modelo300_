class BudgetPage extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "Orçamento";

    /** Budget data. */
    this.gathered_data = {};
  }

  // CANVAS
  // ----------------------------------------

  /**
   * Generates the background for the intro section.
   */
  cv_plusBackground() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('steps');
    let canvas = section.find('.step-intro .step-background canvas');

    /** Setup canvas for animation. */
    let bg = new budget_PlusBackground();
    bg.c = canvas.get(0);
    bg.prepare();

    /** @animate */
    setInterval(() => bg.animate(), 1000/60);
  }

  /**
   * Generates the background for the other steps.
   */
  cv_floatingDotsBackground() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('steps');
    let canvas = section.children('.background').find('canvas');

    /** Setup canvas for animation. */
    let bg = new budget_DotsBackground();
    bg.canvas = canvas.get(0);
    bg.prepare();

    /** @animate */
    bg.animate();
    setInterval(() => bg.animate(), 1000/60);
  }

  // DATA
  // ----------------------------------------

  /**
   * Checks if all the data within the current step is valid.
   *
   * @return {Promise} Resolved if valid.
   */
  data__validate() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('steps');

    /** Get the current step. */
    let current_step = section.find('.step.active');
    let current_step_index = current_step.data('index');
    if (current_step.is('.step-intro')) return Promise.resolve(true);

    /** Get all fields from the current step. */
    let fields = current_step.find('.budget-question');

    /** List validations for each step. */
    let validate = [

      /** Step 1. */
      () => {

        /** Check for at least one field checked. */
        let checked = 0;
        fields.find('.option .field').each(function() {
          if ($(this).is(':checked')) checked++;
        })

        /** @resolve */
        return Promise.resolve((checked === 0) ? false : true);
      },

      /** Step 2. */
      () => {

        /** Check if both fields have their values. */
        let empty = 0;
        fields.find('.field').each(function() {
          if ($(this).val().length === 0) empty++;
        })

        /** @resolve */
        return Promise.resolve((empty === 0) ? true : false);
      }
    ]

    /** @validate */
    let promise = new Promise(resolve => {
      validate[current_step_index - 1]().then(valid => resolve(valid));
    });

    /** @return */
    return promise;
  }

  /**
   * Gathers data from the current step.
   */
  data__gather() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('steps');

    /** Get the current step. */
    let current_step = section.find('.step.active');
    let current_step_index = current_step.data('index');
    if (current_step.is('.step-intro')) return Promise.resolve();

    /** Get all fields from the current step. */
    let fields = current_step.find('.field');

    /** List gathering types for each step. */
    let gather = [

      /** Step 1. */
      () => {

        /** @prepare: Data. */
        $inst.gathered_data['project_type'] = [];

        /** Loop through each option and save the selected value. */
        fields.each(function() {

          /** Get option valud. */
          let option_value = $(this).val().replace('<strong>', '').replace('</strong>', '');

          /** @save */
          if ($(this).is(':checked')) $inst.gathered_data['project_type'].push(option_value);
        });
      },

      /** Step 2. */
      () => {

        /** Loop through each field and save its value. */
        fields.each(function() {

          /** Get field information. */
          let name = $(this).attr('name');
          let value = $(this).val();

          /** @save */
          $inst.gathered_data[name] = value;
        })
      }
    ]

    /** @gather */
    gather[current_step_index - 1]();

    /** @return */
    return Promise.resolve();
  }

  // NAVIGATION
  // ----------------------------------------

  /**
   * @setup: Navigation controllers.
   */
  nav__setup() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('steps');
    let nav_controller = section.find('.nav-controller');

    /** Bind click event to navigation controllers. */
    nav_controller.on('click', function() {
      if ($(this).is('.disabled')) return false;

      /** Get navigation direction. */
      let direction = $(this).data('direction');

      /** @if: ["prev"] = Go back. */
      if (direction === 'prev') return $inst.nav__move(direction);

      /** Validate data. */
      $inst.data__validate().then(valid => {
        if (valid) {

          /** Gather step data. */
          $inst.data__gather().then(() => $inst.nav__move(direction));
        } else {
          notify('Todos os campos são obrigatórios.', 'alizarin');
        }
      })
    })
  }

  /**
   * Changes the active step.
   *
   * @param {String} direction Wether to move to the next or previous step.
   */
  nav__move(direction) {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('steps');

    /** Get current step. */
    let current_step = section.find('.step.active');
    let current_step_index = current_step.data('index');

    /** Get next step index. */
    let next_step_index = (direction === 'next') ? current_step_index + 1 : current_step_index - 1;
    let next_step = section.find('.step[data-index="'+next_step_index+'"]');

    /** Change active step. */
    current_step.removeClass('active');
    setTimeout(() => {
      next_step.addClass('active');

      /** Check for the first or second steps. */
      if (next_step_index === 0) {
        section.children('.background').addClass('hide');
      } else {
        section.children('.background').removeClass('hide');
      }

      /** Check for the last step. */
      if (next_step_index === 3) $inst.yuri__start();
    }, 500);
  }

  // STEPS
  // ----------------------------------------

  /**
   * @setup: Step 1.
   */
  setup__step_1() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('steps');
    let step = section.find('.step-1');

    /** Setup option selection on step 1. */
    step.find('.budget-question .option .option-link').on('click', function() {

      /** Get main information. */
      let this_option = $(this).parents('.option');
      let this_option_value = this_option.data('value');
      let field = this_option.find('.field');

      /** Change option state. */
      if (this_option.is('.active')) {
        this_option.removeClass('active');
      } else {
        this_option.addClass('active');
      }

      /** Change field state */
      if (this_option.is('.active')) {
        field.prop('checked', true);
      } else {
        field.prop('checked', false);
      }
    })
  }

  /**
   * @setup: Step 2.
   */
  setup__step_2() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('steps');
    let step = section.find('.step-2');

    /** Setup range inputs. */
    step.find('.budget-question .range-field[type="range"]').each(function() {

      /** Main variables. */
      let range = $(this);
      let slider = range.parents('.slider');
      let field = slider.find('.field');
      let marks = slider.find('.slider-marks');

      /** List breakpoints. */
      let breakpoints = [0, 250, 500, 750, 1000];

      /** Instantiate range object. */
      range.rangeslider({
        polyfill: false,
        onSlide: function(position, value) {

          /** Get the closes mark value. */
          let closest = breakpoints.reduce((prev, curr) => (Math.abs(curr - value) < Math.abs(prev - value) ? curr : prev));

          /** Get selected mark. */
          let mark_index = breakpoints.indexOf(closest);
          let mark = marks.find('.mark[data-index="'+mark_index+'"]');
          let mark_value = mark.data('value');

          /** Change active mark. */
          if (!mark.is('.active')) {
            marks.find('.mark').removeClass('active');
            mark.addClass('active');
          }

          /** Set selected value to the main field. */
          field.val(mark_value);

          /** Change color attribute. */
          slider.find('.rangeslider__handle').attr('data-color', mark_index);
        },
        onSlideEnd: function(position, value) {

          /** Get the closes mark value. */
          let closest = breakpoints.reduce((prev, curr) => (Math.abs(curr - value) < Math.abs(prev - value) ? curr : prev));

          /** Change range value. */
          slider.find('.rangeslider__handle').addClass('animate');
          range.val(closest).change();
          setTimeout(() => slider.find('.rangeslider__handle').removeClass('animate'), 500);

          /** Get selected mark. */
          let mark_index = breakpoints.indexOf(closest);
          let mark = marks.find('.mark[data-index="'+mark_index+'"]');
          let mark_value = mark.data('value');

          /** Check for invalid value. */
          if (mark_value === 'Pra ontem') {
            $inst.gathered_data.project_time = '01 mês'

            /** Blink handler. */
            setTimeout(() => {
              slider.find('.rangeslider__handle').addClass('blink');
              setTimeout(() => slider.find('.rangeslider__handle').removeClass('blink'), 300);
            }, 500);

            /** Animate the mark. */
            mark.addClass('animate');

            /** Change range value to the next option. */
            setTimeout(() => {
              mark.removeClass('animate');
              slider.find('.rangeslider__handle').addClass('animate');
              range.val(250).change();
              setTimeout(() => slider.find('.rangeslider__handle').removeClass('animate'), 500);
            }, 1000);
          }
        }
      });

      /** Bind click event to marks. */
      marks.find('.mark button').on('click', function() {

        /** Main variables. */
        let mark = $(this).parents('.mark');
        let mark_index = mark.data('index');
        let mark_value = mark.data('value');

        /** Change active mark. */
        if (slider.find('.rangeslider__handle').is('.animate')) return false;
        marks.find('.mark').removeClass('active');
        mark.addClass('active');

        /** Change range value. */
        slider.find('.rangeslider__handle').addClass('animate');
        range.val(breakpoints[mark_index]).change();
        setTimeout(() => slider.find('.rangeslider__handle').removeClass('animate'), 500);

        /** Check for invalid value. */
        if (mark_value === 'Pra ontem') {

          /** Blink handler. */
          setTimeout(() => {
            slider.find('.rangeslider__handle').addClass('blink');
            setTimeout(() => slider.find('.rangeslider__handle').removeClass('blink'), 300);
          }, 500);

          /** Animate the mark. */
          setTimeout(() => {
            mark.addClass('animate');

            /** Change range value to the next option. */
            setTimeout(() => {
              mark.removeClass('animate');
              slider.find('.rangeslider__handle').addClass('animate');
              range.val(250).change();
              setTimeout(() => slider.find('.rangeslider__handle').removeClass('animate'), 500);
            }, 1000);
          }, 250);
        }
      });

      /** Add initial color class. */
      slider.find('.rangeslider__handle').addClass('color-middle').attr('data-color', 2);
    })
  }

  // YURI
  // ----------------------------------------

  /**
   * @setup: General functionalities for Yuri.
   */
  yuri__setup() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('steps');
    let chat_box = section.find('.step-3 .chat[rel="chat-box"] .chat-inner-wrapper');
    let chat_form = section.find('.step-3 .chat[rel="chat-box"] .chat-actions form');

    /** Save chat elements. */
    $inst.chat = {
      box: chat_box,
      form: chat_form,
      stage: 1,
      info: {
        name: false,
        email: false,
        phone: false
      }
    }

    /** Create Yuri. */
    $inst.yuri = new Yuri($inst);

    /** Bind submit event to the form. */
    chat_form.on('submit', function(event) {
      event.preventDefault();
      if (chat_form.is('.disabled')) return false;

      /** Get message. */
      let message = chat_form.find('.field').val().trim();
      if (message !== '') {

        /** Disable the form. */
        chat_form.addClass('disabled');
        chat_form.find('.field').val('').blur().prop('disabled', true);

        /** Setup message template. */
        let template = ''+
        '<div class="header">'+
          '<div class="name">Você</div>'+
        '</div>'+
        '<div class="list">'+
          '<ul><li class="message">'+message+'</li></ul>'+
        '</div>';

        /** Create message group. */
        let message_group = document.createElement('div');
        message_group.innerHTML = template;
        $(message_group).addClass('message-group message-group-user');

        /** Append message to the chat box. */
        chat_box.append(message_group);
        chat_box.get(0).scrollTop = chat_box.get(0).scrollHeight;

        /** Send message to Yuri. */
        setTimeout(() => {

          /** Tell Yuri what the current info and stage are. */
          $inst.yuri.chat_info = $inst.chat.info;
          $inst.yuri.chat_stage = $inst.chat.stage;

          /** Send the message to Yuri. */
          $inst.yuri.receive(message).then(chat_status => {
            $inst.chat.info = chat_status.info;
            $inst.chat.stage = chat_status.stage;

            /** Change the input mask. */
            if ($inst.chat.stage === 3) {

              /** @prepare: Phone mask. */
              let phoneMask = function(val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00000';
              }
              let phoneMaskOptions = {
                onKeyPress: function(val, e, field, options) {
                  $inst.chat.form.find('.field-group .field').mask(phoneMask.apply({}, arguments), options);
                }
              }

              /** @apply: Phone mask. */
              $inst.chat.form.find('.field-group .field').mask(phoneMask, phoneMaskOptions);
            } else {
              $inst.chat.form.find('.field-group .field').unmask();
            }

            /** Re-enable the field for the user to type in. */
            if ($inst.chat.stage !== 4) {
              $inst.chat.form.removeClass('disabled');
              $inst.chat.form.find('.field-group .field').prop('disabled', false).focus();
            }

            /** @finish */
            if ($inst.chat.stage === 4) {

              /** Save gathered information. */
              for (let key in $inst.chat.info) {
                $inst.gathered_data['user_'+key] = $inst.chat.info[key];
              }

              $inst.sendEmail().then(() => {
                $inst.yuri.say_goodbye()
              })
            }
          });
        }, 1000);
      }
    })
  }

  /**
   * Starts the chat functionality, sending the first
   * message and waiting for the user to respond.
   */
  yuri__start() {
    let $inst = this;

    /** Get Yuri. */
    let yuri = $inst.yuri;

    /** Send initial messages. */
    yuri.say(...[
      'Olá, eu sou o Yuri, assistente virtual da 300! 😄',
      'Qual é o seu nome?'
    ]).then(() => {
      $inst.chat.form.removeClass('disabled');
      $inst.chat.form.find('.field-group .field').prop('disabled', false).focus();
    });
  }

  // ACTIONS
  // ----------------------------------------

  /** Sends the e-mail. */
  sendEmail() {
    let formData = this.gathered_data
    let project_type = formData.project_type
    formData.project_type = formData.project_type.join('</li><li style="margin: 0; color: #4E4AF1;">')
    formData.project_type = `<li style="margin: 0; color: #4E4AF1;">${formData.project_type}</li>`

    if (formData.project_time === 'Pra ontem') formData.project_time = '01 mês';

    return new Promise(resolve => {
      $.post(`${window.siteurl}/ajax.php`, {
        action: 'sendEmail',
        scope: 'orcamento',
        formData
      }, res => {

        /** Get UTM. */
        let utm_source   = get_url_param('utm_source') || "";
        let utm_campaign = get_url_param('utm_campaign') || "";
        let utm_medium   = get_url_param('utm_medium') || "";
        let utm_content = get_url_param('utm_content') || "";

        /** Get data. */
        let data = {
          nome: formData['user_name'],
          email: formData['user_email'],
          telefone: formData['user_phone']
        };

        /** Leadpower Integration. */
        console.warn('LeadPower: Sending...');
        $.ajax({
          url: 'https://painel.leadpower.com.br/api.php',
          method: 'POST',
          data: {
            company: '300 Comunicação',
            project: 'Website',
            channel: 'Orçamento',
            name: data.nome,
            email: data.email,
            phone: data.telefone,
            utm_source: utm_source,
            utm_campaign: utm_campaign,
            utm_medium: utm_medium,
            utm_content: utm_content,
            email_list: [
              'igis@trezentos.com.br',
              'raphael@trezentos.com.br',
              'diogo@trezentos.com.br'
            ]
          },
          dataType: 'json',
          success: leadpower_response => {
            if (leadpower_response.status === 'success') {
              console.warn('LeadPower: Success.');
            } else {
              console.warn('LeadPower: Error.', leadpower_response);
            }
          }
        })

        /** Hubspot integration. */
        console.groupCollapsed('Hubspot')
        data.project_type = project_type
        data.project_time = formData.project_time
        data.project_budget = formData.project_budget
        data.pageSlug = 'orcamento';
        data.pageName = 'Orçamento';

        $.post(`${window.siteurl}/ajax.php`, {
          action: 'sendToHubspot',
          data
        }, res => {
          hubspot__submitForm(data)
          console.groupEnd()
        }).done(() => {
          resolve()
        })
      })
    })
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {
    let $inst = this;

    /** @setup: Canvas effects. */
    $inst.cv_plusBackground();
    $inst.cv_floatingDotsBackground();

    /** @setup: Steps functionalities. */
    $inst.setup__step_1();
    $inst.setup__step_2();

    /** @setup: Yuri. */
    $inst.yuri__setup();

    /** @setup: navigation. */
    $inst.nav__setup();
  }
}
