class projects_MainBackground {

  /**
   * Constructor.
   */
  constructor() {
    this.elements = [];
  }

  /**
   * @create: Circle.
   */
  create_circle(x, y, s, dx, dy) {
    let $inst = this;

    /** @return */
    return {
      x: x,
      y: y,
      r: 9 * s,
      w: 4 * s,
      dx: dx,
      dy: dy,
      draw: function(ctx, t) {
        this.x += this.dx;
        this.y += this.dy;

        ctx.beginPath();
        ctx.arc(this.x + + Math.sin((50 + x + (t / 5)) / 100) * 3, this.y + + Math.sin((45 + x + (t / 5)) / 100) * 4, this.r, 0, 2 * Math.PI, false);
        ctx.lineWidth = this.w;
        ctx.strokeStyle = '#2A2A2A';
        ctx.stroke();
      }
    }
  }

  /**
   * @create: Cross.
   */
  create_cross(x, y, s, dx, dy, dr, r) {
    let $inst = this;
    r = r || 0;

    /** @return */
    return {
      x: x,
      y: y,
      s: 20 * s,
      w: 5 * s,
      r: r,
      dx: dx,
      dy: dy,
      dr: dr,
      draw: function(ctx, t) {
        this.x += this.dx;
        this.y += this.dy;
        this.r += this.dr;

        var _this = this;
        var line = function(x, y, tx, ty, c, o) {
          o = o || 0;
          ctx.beginPath();
          ctx.moveTo(-o + ((_this.s / 2) * x), o + ((_this.s / 2) * y));
          ctx.lineTo(-o + ((_this.s / 2) * tx), o + ((_this.s / 2) * ty));
          ctx.lineWidth = _this.w;
          ctx.strokeStyle = c;
          ctx.stroke();
        };

        ctx.save();

        ctx.translate(this.x + Math.sin((x + (t / 5)) / 100) * 5, this.y + Math.sin((10 + x + (t / 5)) / 100) * 2);
        ctx.rotate(this.r * Math.PI / 180);

        line(-1, -1, 1, 1, '#2A2A2A');
        line(1, -1, -1, 1, '#2A2A2A');

        ctx.restore();
      }
    }
  }

  /**
   * @prepare: General settings.
   */
  prepare() {
    let $inst = this;
    $inst.ctx = $inst.canvas.getContext("2d");
    $inst.canvas.width = window.innerWidth;
    $inst.canvas.height = window.innerHeight;
  }

  /**
   * Creates all the circles and crosses to begin the animation.
   */
  init() {
    let $inst = this;

    for (let x = 0; x < $inst.canvas.width; x++) {
      for (let y = 0; y < $inst.canvas.height; y++) {
        if (Math.round(Math.random() * 15100) == 1) {
          var s = ((Math.random() * 5) + 1) / 10;
          if (Math.round(Math.random()) == 1) {
            $inst.elements.push($inst.create_circle(x, y, s, 0, 0));
          } else {
            $inst.elements.push($inst.create_cross(x, y, s, 0, 0, ((Math.random() * 3) - 1) / 10, (Math.random() * 360)));
          }
        }
      }
    }
  }

  /**
   * Animation keyframe.
   */
  animate() {
    let $inst = this;
    $inst.ctx.clearRect(0, 0, $inst.canvas.width, $inst.canvas.height);
    let time = new Date().getTime();
    for (let e in $inst.elements) {
      $inst.elements[e].draw($inst.ctx, time);
    }
  }
}
