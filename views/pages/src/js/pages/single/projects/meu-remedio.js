class MeuRemedioSingle extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "Meu Remédio (App)";

    /** Main variables. */
    this.demo = {}
    this.images_preview = {}
  }

  // MAIN SETUPS
  // ----------------------------------------

  /**
   * @setup: Services.
   */
  setup__services() {
    let $inst = this;

    /** Bind scroll event to the document. */
    $(document).on('scroll', () => $inst.appear__services());
    $inst.appear__services();
  }

  /**
   * @setup: Cards.
   */
  setup__cards() {
    let $inst = this;

    /** Bind scroll event to the document. */
    $(document).on('scroll', () => $inst.appear__cards());
    $inst.appear__cards()
  }

  // APPEAR
  // ----------------------------------------

  /**
   * @appear: Services.
   */
  appear__services() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('services').find('.section-blocks');
    let blocks = section.find('.blocks');
    let line = section.find('.line');

    /** Get the current scroll position. */
    let scroll_position = $(window).scrollTop() + $(window).height();
    scroll_position = scroll_position - 200;

    /** Get the section offset. */
    let section_offset = section.offset().top;
    $(window).on('resize', () => section_offset = section.offset().top);

    /** Check if the current scroll position is higher than the section offset. */
    if (scroll_position > section_offset && !section.is('.animated')) {
      section.addClass('animated');

      /** Calculate the delay between each block. */
      let blocks_count = blocks.find('.block').length;
      let delay_between = Number((2000 / blocks_count).toFixed(2));

      /** Animate the line. */
      line.addClass('show');

      /** Animate the blocks. */
      for (let x = 0; x < blocks_count; x++) {

        /** Calculate the delay. */
        let delay = delay_between * x;

        /** Animate the block. */
        setTimeout(() => blocks.find('.block[data-index="'+x+'"]').addClass('show'), delay);
      }
    }
  }

  /**
   * @appear: Cards.
   */
  appear__cards() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('cards');
    let cards = section.find('.cards');

    /** Get the current scroll position. */
    let scroll_position = $(window).scrollTop() + $(window).height();
    scroll_position = scroll_position - 200;

    /** Get the section offset. */
    let section_offset = section.offset().top + 800;
    $(window).on('resize', () => section_offset = section.offset().top);

    /** Check if the current scroll position is higher than the section offset. */
    if (scroll_position > section_offset && !section.is('.animated')) {
      section.addClass('animated');

      /** Get cards count. */
      let cards_count = cards.find('.card').length;

      /** Animate tiles opening. */
      for (let x = 0; x < cards_count; x++) {

        /** Setup delay. */
        let delay = 150 * x;

        /** Get the current card. */
        let card = cards.find('.card[data-index="'+(x+1)+'"]');

        /** Animate the entrance for the current card. */
        setTimeout(() => card.addClass('show'), delay);
      }
    }
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {
    let $inst = this;

    /** Main setups. */
    $inst.setup__services();
    $inst.setup__cards();
  }
}
