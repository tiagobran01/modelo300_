
// MAIN FUNCTIONS
// ------------------------------------------------------------

/**
 * Opensa  modal and triggers entrances.
 *
 * @param {string} key Modal key.
 */
function modal_open(key) {

  /** Get modal information. */
  let modal = $('.modal[data-key="'+key+'"]');

  /** Open modal. */
  modal.addClass('open');
  setTimeout(() => modal.trigger('opened'), 1000);
}

/**
 * Closes a modal.
 *
 * @param {string} key Modal key.
 */
function modal_close(key) {

  /** Get modal information. */
  let modal = $('.modal[data-key="'+key+'"]');

  /** Close modal. */
  modal.removeClass('open');
  setTimeout(() => modal.trigger('closed'), 1000);
}

/**
 * Bind click to toggle modals state.
 */
$('[rel="modal_toggler"]').on('click', function() {

  /** Get modal key. */
  let key = $(this).data('modal');

  /** Open modal. */
  modal_open(key);
})

/**
 * Close modals.
 */
$('.modal [rel="close"]').on('click', function() {

  /** Get modal key. */
  let key = $(this).parents('.modal').data('key');

  /** Close modal. */
  modal_close(key);
})
$('.modal').on('click', function(e) {

  /** Get modal information. */
  let key = $(this).data('key');
  let wrapper = $(this).find('.wrapper');

  /** Check for the click location. */
  if (!wrapper.is(e.target) && wrapper.has(e.target).length === 0) modal_close(key);
})
$(document).keydown(function(e) {
  if (e.keyCode === 27) {

    /** Get active modal. */
    let modal = $('.modal.open');
    if (modal.length === 0) return false;

    /** Get modal information. */
    let key = modal.data('key');

    /** Close modal. */
    modal_close(key);
  }
})
