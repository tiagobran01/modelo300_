class PlazzaPremiumLandingPageSingle extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "Plazza Premium (Landing Page)";

    /** Main variables. */
    this.demo = {}
    this.images_preview = {}
  }

  // MAIN SETUPS
  // ----------------------------------------

  /**
   * @setup: Services.
   */
  setup__services() {
    let $inst = this;

    /** Bind scroll event to the document. */
    $(document).on('scroll', () => $inst.appear__services());
    $inst.appear__services();
  }

  /**
   * @setup: Demo.
   *
   * @return {Promise} Resolved when finished.
   */
  setup__demo_scroll() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('demo');
    let wrapper = section.find('.section-wrapper');
    let frame   = wrapper.find('.frame');
    let content = wrapper.find('.content');

    /** Set the initial height to the section. */
    section.height(frame.height());

    /** Update content image source. */
    let image_src = content.find('img').attr('src');
    content.find('img').attr('src', '').attr('src', image_src);

    /** Continue when the image has finished loading. */
    let promise = new Promise(resolve => {
      content.find('img').on('load', function() {

        /** @save: Bleed. */
        $inst.demo.bleed = $inst.demo__calculate_bleed(frame);

        /** @save: Frame margin. */
        $inst.demo.frame_margin = (frame.height() - content.height()) - $inst.demo.bleed;

        /** Apply section height. */
        let section_height = (frame.outerHeight() + content.get(0).scrollHeight) - content.outerHeight();
        section.height(section_height + $inst.demo.frame_margin);

        /** Update saved information on resize. */
        $(window).on('resize', function() {

          /** @update: Bleed. */
          $inst.demo.bleed = $inst.demo__calculate_bleed(frame);

          /** Apply section height. */
          section_height = (frame.outerHeight() + content.get(0).scrollHeight) - content.outerHeight();
          section.height(section_height + $inst.demo.frame_margin);

          /** Update scroll position. */
          $inst.demo__update_scroll(wrapper);
        });

        /** Bind scroll event to the document. */
        $(document).on('scroll', () => $inst.demo__update_scroll(wrapper, content));
        content.scrollTop(content.get(0).scrollHeight);

        /** @resolve */
        resolve();
      })
    })

    /** @return */
    return promise;
  }

  // SECTION: DEMO
  // ----------------------------------------

  /**
   * Calculates section bleed.
   *
   * @param  {Object} frame Demo frame element.
   * @return {Number}       Bleed for vertically centering the section.
   */
  demo__calculate_bleed(frame) {
    let $inst = this;

    /** @calc */
    let vh = $(window).height();
    let fh = frame.height();
    let bleed = (vh - fh) / 2;

    /** @return */
    return bleed;
  }

  /**
   * Updates the scroll and wrapper position.
   *
   * @param {Object} wrapper Section wrapper element.
   */
  demo__update_scroll(wrapper) {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('demo');
    let frame = wrapper.find('.frame');
    let content = wrapper.find('.content');

    /** Get scroll information. */
    let scroll_position = $(window).scrollTop();
    let section_offset = (section.offset().top + 200) - $inst.demo.bleed;
    let extra_scroll = scroll_position - section_offset;

    /** Get content information. */
    let section_height = (section.offset().top + section.outerHeight()) - $(window).height();
    let is_past = (scroll_position + 203 - $inst.demo.bleed) >= section_height;

    /** Update scroll and wrapper position. */
    if ((scroll_position > section_offset) && !is_past) {

      /**
       * The current scroll position is highet than the section offset.
       * Fix the wrapper and scroll the content along with the page.
       */
      wrapper.addClass('fixed').css('top', $inst.demo.bleed);

      /** Enable the scroll for the content. */
      content.scrollTop(extra_scroll);
    } else if (!is_past) {

      /**
       * The current scroll position is lower than the section offset.
       * Reposition the wrapper and the content's scroll point.
       */
      wrapper.removeClass('fixed').css('top', 200);
      content.scrollTop(0);
    } else {

      /**
       * The current scroll position is higher than the content's scrollable area.
       * Let the section move with the page.
       */
      wrapper.removeClass('fixed').css('top', 'auto').css('bottom', 200);
    }
  }

  // APPEAR
  // ----------------------------------------

  /**
   * @appear: Services.
   */
  appear__services() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('services').find('.section-blocks');
    let blocks = section.find('.blocks');
    let line = section.find('.line');

    /** Get the current scroll position. */
    let scroll_position = $(window).scrollTop() + $(window).height();
    scroll_position = scroll_position - 200;

    /** Get the section offset. */
    let section_offset = section.offset().top;
    $(window).on('resize', () => section_offset = section.offset().top);

    /** Check if the current scroll position is higher than the section offset. */
    if (scroll_position > section_offset && !section.is('.animated')) {
      section.addClass('animated');

      /** Calculate the delay between each block. */
      let blocks_count = blocks.find('.block').length;
      let delay_between = Number((2000 / blocks_count).toFixed(2));

      /** Animate the line. */
      line.addClass('show');

      /** Animate the blocks. */
      for (let x = 0; x < blocks_count; x++) {

        /** Calculate the delay. */
        let delay = delay_between * x;

        /** Animate the block. */
        setTimeout(() => blocks.find('.block[data-index="'+x+'"]').addClass('show'), delay);
      }
    }
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {
    let $inst = this;

    /** Main setups. */
    Promise.all([
      $inst.setup__demo_scroll()
    ]).then(() => {
      $inst.setup__services();
    })
  }
}
