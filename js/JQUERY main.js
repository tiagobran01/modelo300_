$flag = true;
$counter = 1;

$(window).on('wheel',function(event){
    if($flag)
    {
        $flag = false;
        if(event.originalEvent.deltaY > 0)
        {
            $counter++;
            
            if($counter==1)
            $counter++;

            if($counter==2)
            {
                $(".colorbox1").animate({'top':'-100%'});
                $(".colorbox2").animate({'top':'0%'});
                $(".red_info").animate({'top':'-100%'},700);
                $(".orange_info").animate({'top':'0%'},700);
                $(".img1").animate({'top':'-50%'},700);
                $(".img2").animate({'top':'50%'});
            }

            if($counter==3)
            {
                $(".colorbox2").animate({'top':'-100%'});
                $(".colorbox3").animate({'top':'0%'});
                $(".orange_info").animate({'top':'-100%'},700);
                $(".green_info").animate({'top':'0%'},700);
                $(".img2").animate({'top':'-50%'},700);
                $(".img3").animate({'top':'50%'});
            }

            if($counter>3)
            $counter=4;
        }
    }
});