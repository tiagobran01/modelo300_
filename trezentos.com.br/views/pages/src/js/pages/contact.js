class ContactPage extends PageController {

  /** Main information for this page. */
  constructor() {
    super()
    this.pageTitle = 'Contato'
  }

  // SETUPS
  // ----------------------------------------

  /** @setup: Contact form. */
  setup__contactForm() {

    /** Get main elements. */
    let thePage = this.currentPage
    let theForm = thePage.find('.form')

    /** Bind submit event to the form. */
    theForm.on('submit', function(e) {
      e.preventDefault()

      if (form_validate(theForm)) {

        /** Gather form data. */
        let formData = {}
        theForm.find('.field-group').each(function() {
          let group = $(this)
          let fieldName = group.data('name')
          let fieldValue = group.find('.field').val()
          formData[fieldName] = fieldValue
        })

        /** @format: Form data. */
        let userName = formData['nome'].split(' ')
        formData['nome'] = []
        for (let part of userName) {
          formData['nome'].push(part.slice(0, 1).toUpperCase() + part.slice(1).toLowerCase())
        }
        formData['nome'] = formData['nome'].join(' ')

        /** @request */
        let loader = progress_screen('Enviando mensagem...')
        $.post(`${window.siteurl}/ajax.php`, {
          action: 'sendEmail',
          scope: 'contato',
          formData
        }, res => {

          /** Get UTM. */
          let utm_source   = get_url_param('utm_source') || "";
          let utm_campaign = get_url_param('utm_campaign') || "";
          let utm_medium   = get_url_param('utm_medium') || "";
          let utm_content = get_url_param('utm_content') || "";

          /** Get data. */
          let data = {
            nome: formData['nome'],
            email: formData['email'],
            telefone: formData['telefone']
          };

          /** Leadpower Integration. */
          console.warn('LeadPower: Sending...');
          $.ajax({
            url: 'https://painel.leadpower.com.br/api.php',
            method: 'POST',
            data: {
              company: '300 Comunicação',
              project: 'Website',
              channel: 'Contato',
              name: data.nome,
              email: data.email,
              phone: data.telefone,
              utm_source: utm_source,
              utm_campaign: utm_campaign,
              utm_medium: utm_medium,
              utm_content: utm_content,
              email_list: [
                'igis@trezentos.com.br',
                'raphael@trezentos.com.br',
                'diogo@trezentos.com.br'
              ]
            },
            dataType: 'json',
            success: leadpower_response => {
              if (leadpower_response.status === 'success') {
                console.warn('LeadPower: Success.');
              } else {
                console.warn('LeadPower: Error.', leadpower_response);
              }
            }
          })

          /** Hubspot integration. */
          console.groupCollapsed('Hubspot')
          data.assunto  = formData['assunto'];
          data.mensagem = formData['mensagem'];
          data.pageSlug = 'contato';
          data.pageName = 'Contato';
          $.post(`${window.siteurl}/ajax.php`, {
            action: 'sendToHubspot',
            data
          }, res => {
            hubspot__submitForm(data)
            console.groupEnd()

            /** Close progress screen. */
            loader.trigger('close', {
              status: 'success',
              message: 'Mensagem enviada!',
              timeout: 3000
            });
          })
        })
      }
    })
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {

    /** Main setups. */
    this.setup__contactForm()
  }
}
