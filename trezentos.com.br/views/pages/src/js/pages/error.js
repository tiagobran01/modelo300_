class ErrorPage extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "404";
  }

  // CANVAS
  // ----------------------------------------

  /**
   * @setup: Sea background.
   */
  cv__seaBackground() {
    let $inst = this;

    /** Get main elements. */
    let bg = $inst.currentPage.find('.background');

    /** Setup background. */
    error_SeaBackground(bg);
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {
    let $inst = this;

    /** @setup: Background animation. */
    $inst.cv__seaBackground();
  }
}
