
// INTRO
// ----------------------------------------

class budget_PlusBackground {

  /**
   * Constructor.
   */
  constructor() {
    this.plusses = [];
    this.count = 1000;
    this.tick = 43;
    this.tickMax = 43;
  }

  // UTILITIES
  // ----------------------------------------

  /**
   * Get random number between ranges.
   */
  random(a, b) {
    return ~~((Math.random()*(b-a+1))+a);
  }

  // INITIAL SETTINGS
  // ----------------------------------------

  /**
   * Prepare canvas element and its properties.
   */
  prepare() {
    this.ctx = this.c.getContext('2d');
    this.cw  = this.c.width = window.innerWidth;
    this.ch  = this.c.height = window.innerHeight;
  }

  // ANIMATION STEPS
  // ----------------------------------------

  /**
   * Generates plusses.
   */
  createPlus() {
    let $inst = this;

    /** Attempt to create plus. */
    if ($inst.plusses.length < $inst.count) {
      if ($inst.tick >= $inst.tickMax) {

        /** Create a new plus. */
        let plus = {
          x: $inst.cw / 2,
          y: $inst.ch * .8,
          vx: ($inst.random(50, 300)-50) / 12,
          vy: -($inst.random(150, 200)) / 9,
          lightness: $inst.random(0, 50),
          alpha: .1,
          fade: .015,
          scale: 1,
          growth: .06,
          rotation: $inst.random(0, Math.PI*2),
          spin: ($inst.random(0, 100)-50) / 300,
        }

        /** @push */
        $inst.plusses.push(plus);
        $inst.tick = 0;
      } else {
        $inst.tick++;
      }
    }
  }

  /**
   * Updates a specific plus.
   */
  updatePlus(i) {
    let $inst = this;

    /** Update plus propeties. */
    $inst.plusses[i].x  += $inst.plusses[i].vx;
    $inst.plusses[i].y  += $inst.plusses[i].vy;
    $inst.plusses[i].vy += .15 * $inst.plusses[i].scale;
    if ($inst.plusses[i].alpha < 1) {
      $inst.plusses[i].alpha += $inst.plusses[i].fade;
    }
    $inst.plusses[i].scale += $inst.plusses[i].growth;
    $inst.plusses[i].rotation += $inst.plusses[i].spin;

    if ($inst.plusses[i].y - 30 >= $inst.ch) {

      /** Create a new plus. */
      let plus = {
        x: $inst.cw / 2,
        y: $inst.ch * .8,
        vx: ($inst.random(60, 150)-50) / 12,
        vy: -($inst.random(80, 200)) / 9,
        lightness: $inst.random(0, 50),
        alpha: .1,
        fade: .015,
        scale: 1,
        growth: .06,
        rotation: $inst.random(0, Math.PI*2),
        spin: ($inst.random(0, 100)-50) / 300,
      }
      $inst.plusses[i] = plus;
    }
  }

  renderPlus(i) {
    let $inst = this;
    let plus = $inst.plusses[i];

    /** @render */
    $inst.ctx.save();
    $inst.ctx.translate(plus.x, plus.y);
    $inst.ctx.scale(plus.scale, plus.scale);
    $inst.ctx.rotate(plus.rotation);

    $inst.ctx.fillStyle = 'hsla(0, 0%, '+plus.lightness+'%, '+plus.alpha+')';
    $inst.ctx.beginPath();
    $inst.ctx.rect(-3, -1, 6, 2);
    $inst.ctx.rect(-1, -3, 2, 6);
    $inst.ctx.fill();
    $inst.ctx.restore();
  }

  /**
   * Updates plusses properties.
   */
  updatePlusses() {
    let $inst = this;

    /** Loop through plusses. */
    for (let x = 0; x < $inst.plusses.length; x++) {
      $inst.updatePlus(x);
    }
  }

  /**
   * Renders plusses.
   */
  renderPlusses() {
    let $inst = this;
    for (let x = 0; x < $inst.plusses.length; x++) {
      $inst.renderPlus(x);
    }
  }

  /**
   * Animation keyframe.
   */
  animate() {
    let $inst = this;
    $inst.ctx.clearRect(0, 0, $inst.cw, $inst.ch);
    $inst.createPlus();
    $inst.updatePlusses();
    $inst.renderPlusses();
  }
}

// GENERAL
// ----------------------------------------

class budget_DotsBackground {

  /**
   * Constructor.
   */
  constructor() {
    this.circles = [];

    /** Settings. */
    this.opacity = 0.15;
    this.colors = [
      'rgba(207, 25, 26, '+this.opacity+')',
      'rgba(189, 195, 199, '+this.opacity+')',
      'rgba(241, 196, 15, '+this.opacity+')',
      'rgba(231, 76, 60, '+this.opacity+')',
      'rgba(231, 76, 60, '+this.opacity+')'
    ];
    this.minSize = 1;
    this.maxSize = 4;
    this.numCircles = 100;
    this.minSpeed = -2;
    this.maxSpeed = 4;
    this.expandState = false;
    this.xVal = 10;
  }

  /**
   * @prepare: General settings.
   */
  prepare() {
    let $inst = this;
    $inst.context = $inst.canvas.getContext("2d");
    $inst.canvas.width = window.innerWidth;
    $inst.canvas.height = window.innerHeight;
    $inst.build_array();
  }

  /**
   * Builds an array of circles.
   */
  build_array() {
    let $inst = this;

    /** Generate the specified number of circles. */
    for (let i = 0; i < $inst.numCircles; i++) {
      let color = Math.floor(Math.random() * ($inst.colors.length - 1 + 1)) + 1;
      let left = Math.floor(Math.random() * ($inst.canvas.width - 0 + 1)) + 0;
      let top = Math.floor(Math.random() * ($inst.canvas.height - 0 + 1)) + 0;
      let size = Math.floor(Math.random() * ($inst.maxSize - $inst.minSize + 1)) + $inst.minSize;
      let leftSpeed = (Math.floor(Math.random() * ($inst.maxSpeed - $inst.minSpeed + 1)) + $inst.minSpeed)/10;
      let topSpeed = (Math.floor(Math.random() * ($inst.maxSpeed - $inst.minSpeed + 1)) + $inst.minSpeed)/10;
      let expandState = $inst.expandState;

      while (leftSpeed == 0 || topSpeed == 0) {
        leftSpeed = (Math.floor(Math.random() * ($inst.maxSpeed - $inst.minSpeed + 1)) + $inst.minSpeed)/10,
        topSpeed = (Math.floor(Math.random() * ($inst.maxSpeed - $inst.minSpeed + 1)) + $inst.minSpeed)/10;
      }

      /** Setup circle object. */
      let circle = {
        color: color,
        left: left,
        top: top,
        size: size,
        leftSpeed: leftSpeed,
        topSpeed: topSpeed,
        expandState: expandState
      };

      /** @push */
      $inst.circles.push(circle);
    }
  }

  /**
   * Builds the background.
   */
  build() {
    let $inst = this;

    for (var h = 0; h < $inst.circles.length; h++) {
      var curCircle = $inst.circles[h];
      $inst.context.fillStyle = $inst.colors[curCircle.color-1];
      $inst.context.beginPath();

      if (curCircle.left > $inst.canvas.width+curCircle.size) {
        curCircle.left = 0-curCircle.size;
        $inst.context.arc(curCircle.left, curCircle.top, curCircle.size, 0, 2 * Math.PI, false);
      } else if (curCircle.left < 0-curCircle.size) {
        curCircle.left = $inst.canvas.width+curCircle.size;
        $inst.context.arc(curCircle.left, curCircle.top, curCircle.size, 0, 2 * Math.PI, false);
      } else {
        curCircle.left = curCircle.left+curCircle.leftSpeed;
        $inst.context.arc(curCircle.left, curCircle.top, curCircle.size, 0, 2 * Math.PI, false);
      }

      if (curCircle.top > $inst.canvas.height+curCircle.size) {
        curCircle.top = 0-curCircle.size;
        $inst.context.arc(curCircle.left, curCircle.top, curCircle.size, 0, 2 * Math.PI, false);
      } else if (curCircle.top < 0-curCircle.size) {
        curCircle.top = $inst.canvas.height+curCircle.size;
        $inst.context.arc(curCircle.left, curCircle.top, curCircle.size, 0, 2 * Math.PI, false);
      } else {
        curCircle.top = curCircle.top+curCircle.topSpeed;
        if (curCircle.size != $inst.maxSize && curCircle.size != $inst.minSize && curCircle.expandState == false) {
          curCircle.size = curCircle.size-0.1;
        } else if (curCircle.size != $inst.maxSize && curCircle.size != $inst.minSize && curCircle.expandState == true) {
          curCircle.size = curCircle.size+0.1;
        } else if (curCircle.size == $inst.maxSize && curCircle.expandState == true) {
          curCircle.expandState = false;
          curCircle.size = curCircle.size-0.1;
        } else if (curCircle.size == $inst.minSize && curCircle.expandState == false) {
          curCircle.expandState = true;
          curCircle.size = curCircle.size+0.1;
        }

        curCircle.size = Number(curCircle.size.toFixed(1));

        $inst.context.arc(curCircle.left, curCircle.top, curCircle.size, 0, 2 * Math.PI, false);
      }

      $inst.context.closePath();
      $inst.context.fill();
      $inst.context.ellipse;
    }
  }

  /**
   * Animation keyframe.
   */
  animate() {
    let $inst = this;
    $inst.context.clearRect(0, 0, $inst.canvas.width, $inst.canvas.height);
    $inst.xVal++;
    $inst.build();
  }
}
