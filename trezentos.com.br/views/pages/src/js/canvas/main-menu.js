class mainMenu_Background {

  /**
   * Constructor.
   */
  constructor(options) {
    this.$el = options.el;
    this.time = 0;
    this.bindAll();
    this.init();
  }

  /**
   * Bind particles.
   */
  bindAll() {
    this.render = this.render.bind(this);
    this.resize = this.resize.bind(this);
  }

  /**
   * Init.
   */
  init() {
    this.textureLoader = new THREE.TextureLoader();
    this.camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 2000 );
    this.camera.position.z = 20;
    this.camera.position.y = 150;
    this.camera.lookAt(new THREE.Vector3(0, 0, 0));

    this.scene = new THREE.Scene();

    this.renderer = new THREE.WebGLRenderer({ alpha: true });
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize( window.innerWidth, window.innerHeight );
    this.$el.appendChild( this.renderer.domElement );


    this.createParticles();
    this.bindEvents();
    this.resize();
    this.render();
  }

  /**
   * Generates particles.
   */
  createParticles() {
    const plane = new THREE.PlaneBufferGeometry(500, 250, 250, 125);

    const textureLoader = new THREE.TextureLoader();
    textureLoader.crossOrigin = '';

    let texture_path = $(this.$el).attr('data-spark');

    const material = new THREE.ShaderMaterial({
      uniforms: {
        time: {
          value: 1.0
        },
        texture: {
          value: textureLoader.load(texture_path)
        },
        resolution: {
          value: new THREE.Vector2()
        }
      },

      vertexShader: document.getElementById('mainMenu-vertex-shader').textContent,
      fragmentShader: document.getElementById('mainMenu-fragment-shader').textContent,
      blending: THREE.AdditiveBlending,
      depthTest: false,
      transparent: true
    });

    this.particles = new THREE.Points(plane, material);
    this.particles.rotation.x = this.degToRad(-90);

    this.scene.add(this.particles);
  }

  /**
   * Bind events.
   */
  bindEvents() {
    window.addEventListener('resize', this.resize);
  }

  /**
   * Update canvas dimensions.
   */
  resize() {
    const w = window.innerWidth;
    const h = window.innerHeight;
    this.renderer.setSize(w,h);
    this.camera.aspect = w/h;
    this.camera.updateProjectionMatrix();
  }

  /**
   * Moves particles.
   */
  moveParticles() {
    this.particles.material.uniforms.time.value = this.time;
  }

  /**
   * Renders animation.
   */
  render() {
    requestAnimationFrame(this.render);
    this.time += .01;

    this.moveParticles();
    this.renderer.render(this.scene, this.camera);
  }

  /**
   * Utility.
   */
  degToRad(angle) {
      return angle * Math.PI / 180;
  }
}
