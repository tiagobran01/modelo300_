
// MAIN ACTIONS
// ----------------------------------------

/**
 * Opens the menu.
 */
function menu_open() {

  /** Get the menu. */
  let menu = $('#main-menu');

  /** Open the menu. */
  menu.addClass('open');
  $('body').addClass('modal-opened');

  /** Get the active item. */
  let active_item = $('#main-menu .navigation .item.active');

  /** Move line to the active item. */
  menu__move_line(active_item);
}

/**
 * Closes the menu.
 */
function menu_close() {

  /** Get main elements. */
  let menu = $('#main-menu');
  let line = menu.find('[rel="interactive-line"]');

  /** Close the menu. */
  menu.removeClass('open');
  $('body').removeClass('modal-opened');

  /** Move the line back to its original state. */
  line.removeAttr('data-style').css('transform', 'scaleY(0)');
}

/**
 * Moves the menu line to a specific position.
 */
function menu__move_line(element) {

  /** Get the line. */
  let line = $('#main-menu [rel="interactive-line"]');

  /** Declare main variables. */
  let element_style, element_position, element_height;

  /** Get element information. */
  if (element.length !== 0) {
    element_style = element.data('style');
    if (!element_style) element_style = 'white';
    element_position = element.offset().top;
    element_height = element.height() / 1000;
  } else {
    element_style = 'white';
    element_position = 0;
    element_height = 0;
  }

  /** Subtract the element position from the menu position. */
  let menu_position = $('#main-menu').offset().top;
  element_position = (element_position !== 0) ? element_position - menu_position : element_position;

  /** Calculate the line position. */
  let menu_height = $(window).height();
  let line_position = Number((element_position / menu_height).toFixed(2)) + element_height;

  /** Move the line to its position. */
  line.attr('data-style', element_style).css('transform', 'scaleY('+line_position+')');
}

// MAIN SETUPS
// ----------------------------------------

/** Setup canvas animation. */
try {
  let menu_background = $('#main-menu .background .canvas').get(0);
  let bg = new mainMenu_Background({el: menu_background});
} catch(e) {}

/** @setup: Hover effect. */
$('#main-menu .item').on('mouseenter', function() {
  let layer = $(this).data('layer')
  $('#main-menu .background .layer').removeClass('active')
  $(`#main-menu .background .layer-${layer}`).addClass('active')
  $('#main-menu').attr('data-layer', layer)
})

$('#main-menu .item').on('mouseleave', function() {
  $('#main-menu .background .layer').removeClass('active')
  $('#main-menu').removeAttr('data-layer')
})

;(function() {
  let menu = $('#main-menu .content')
  let menuItem = menu.find('.item')
  let vw = $(window).width()
  let vh = $(window).height()

  $(window).on('mousemove', function(e) {
    var offsetX = 0.5 - e.pageX / vw,
        offsetY = 0.5 - e.pageY / vh,
        dy = e.pageY - vh / 2,
        dx = e.pageX - vw / 2,
        theta = Math.atan2(dy, dx),
        angle = theta * 180 / Math.PI - 90,
        offsetPoster = menu.data('offset') || 20,
        transformPoster = 'translate3d(0, ' + -offsetX * offsetPoster + 'px, 0) rotateX(' + (-offsetY * offsetPoster) + 'deg) rotateY(' + (offsetX * (offsetPoster * 2)) + 'deg)';

    if (angle < 0) angle = angle + 360;
    menu.css('transform', transformPoster);
    menuItem.each(function() {
      var $this = $(this),
          offsetLayer = $this.data('offset') || 10,
          transformLayer = 'translate3d(' + offsetX * offsetLayer + 'px, ' + offsetY * offsetLayer + 'px, 20px)';

      $this.css('transform', transformLayer);
    });
  });
}());

/**
 * Bind click event to menu togglers.
 */
$('[rel="menu-toggler"]').on('click', menu_open);

/**
 * Bind click event to menu's close button.
 */
$('#main-menu .close a').on('click', menu_close);

/** Close menu mobile. */
$('#main-menu [rel="close"]').on('click', () => menu_close());

// INTERACTIVE LINE
// ----------------------------------------

/**
 * @move To the hovered item.
 */
$('#main-menu .navigation .item a').on('mouseenter', function() {

  /** Get the current item. */
  let item = $(this).parents('.item');

  /** Move the line to the current item. */
  menu__move_line(item);
});

/**
 * @move To the social links wrapper.
 */
$('#main-menu .social-links .item a').on('mouseenter', function() {

  /** Get the wrapper. */
  let social_links = $(this).parents('.social-links');

  /** Move the line to the wrapper. */
  menu__move_line(social_links);
});

/**
 * @move Back to the active item.
 */
$('#main-menu .navigation .item a, #main-menu .social-links').on('mouseleave', function() {

  /** Get the active item. */
  let active_item = $('#main-menu .navigation .item.active');

  /** Move the line to the active item. */
  menu__move_line(active_item);
});
