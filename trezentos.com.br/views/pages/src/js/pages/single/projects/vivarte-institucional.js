class VivarteInstitucionalSingle extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "Vivarte (Publicidade)";
  }

  // MAIN SETUPS
  // ----------------------------------------

  /**
   * @setup: Cards.
   */
  setup__cards() {
    let $inst = this;

    /** Bind scroll event to the document. */
    $(document).on('scroll', () => $inst.appear__cards());
    $inst.appear__cards()
  }

  // APPEAR
  // ----------------------------------------

  /**
   * @appear: Cards.
   */
  appear__cards() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('social');
    let phones = section.find('.phones');

    /** Get the current scroll position. */
    let scroll_position = $(window).scrollTop() + $(window).height();
    scroll_position = scroll_position - 200;

    /** Get the section offset. */
    let section_offset = section.offset().top + 500;
    $(window).on('resize', () => section_offset = section.offset().top);

    /** Check if the current scroll position is higher than the section offset. */
    if (scroll_position > section_offset && !section.is('.animated')) {
      section.addClass('animated');

      /** Get cards count. */
      phones.find('.phone').addClass('visible');
    }
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {
    let $inst = this;

    /** Main setups. */
    $inst.setup__cards();
  }
}
