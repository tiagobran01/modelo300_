class MaristelaBrandingSingle extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "Maristela (Branding)";

    /** Main variables. */
    this.demo = {}
    this.images_preview = {}
  }

  // MAIN SETUPS
  // ----------------------------------------

  /**
   * @setup: Videos.
   */
  setup__videos() {
    let $inst = this;

    /** Get main elements. */
    let section    = $inst.section('videos');
    let player     = section.find('.gallery .gallery-player');
    let contents   = section.find('.gallery .gallery-contents');
    let thumbnails = section.find('.gallery .gallery-thumbnails');

    /** Set the contents height. */
    let active_content_tab = contents.find('.item.active');
    let active_content_tab_height = active_content_tab.outerHeight();
    contents.height(active_content_tab_height);

    /** Bind click event to the thumbnails. */
    thumbnails.find('.item .item-link').on('click', function() {
      if ($(this).parents('.item').is('.active')) return false;

      /** Get item information. */
      let item = $(this).parents('.item');
      let item_index = item.data('index');
      let item_video = item.data('video');

      /** Change active item. */
      thumbnails.find('.item').removeClass('active');
      item.addClass('active');

      /** Change active player. */
      player.find('.link').attr('data-video', item_video);
      player.find('.images .item').removeClass('active');
      setTimeout(() => player.find('.images .item[data-index="'+item_index+'"]').addClass('active'), 500);

      /** @get: Selected content tab. */
      let selected_content = contents.find('.item[data-index="'+item_index+'"]');
      let selected_content_height = selected_content.outerHeight();

      /** Change active content. */
      contents.find('.item').removeClass('active');
      selected_content.addClass('active');
      contents.animate({
        height: selected_content_height
      },{
        duration: 1000,
        easing: $.bez([0.5, 0, 0, 1])
      })
    })

    /** Get the video modal. */
    let modal = section.find('.modal-video');

    /** Bind click event to the video player. */
    player.find('.link').on('click', function() {

      /** Get the video source. */
      let video_src = $(this).attr('data-video');

      /** Apply the video source and open the modal. */
      modal.find('iframe').attr('src', video_src);
      modal.find('iframe').on('load', () => modal.find('.video-wrapper').addClass('show'));
      modal.addClass('open');

      /** Remove the video source when the modal is closed. */
      modal.on('closed', () => modal.find('iframe').attr('src', ''));
    })
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {
    let $inst = this;

    /** Main setups. */
    $inst.setup__videos();
  }
}
