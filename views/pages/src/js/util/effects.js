
/**
 * Makes elements appear as the user scrolls down.
 */
if ($(window).width() < 1024) {
  function appear() {

    /** Get main variables. */
    let vh = $(window).height();
    let scroll = $('#app').scrollTop();

    /** @setup */
    $("[appear]").each(function() {
      let el = $(this);

      /** Get element position in which the event should be triggered. */
      let on = el.attr("appear");
      if (on.indexOf("vh") != -1) {
        on = (vh / (100 / parseInt(on.replace("vh", ""))));
      } else {
        on = Number(on);
      }

      /** Get additional options. */
      let delay = (el.attr("appear-delay")) ? Number(el.attr("appear-delay")) : 0;
      let delay_target = (el.attr("appear-target-delay")) ? Number(el.attr("appear-target-delay")) : 0;
      let done = (el.attr("appear-done")) ? Number(el.attr("appear-done")) : false;

      /** Get offset from top. */
      let offset = el.offset().top;
      let attr = el.attr('data-offset');

      /** Set attr offset top. */
      if (typeof attr === 'undefined') {
        el.attr('data-offset', (offset + on));
      }

      // TARGET (?)
      // ----------------------------------------

      /** Check for a target key. */
      let target_key = el.attr("appear-target");
      if (target_key) {
        if ((scroll + vh) > (offset + on)) {
          let targets = el.find('[appear-key="'+target_key+'"]');

          /** Loop through each target. */
          setTimeout(() => {
            targets.each(function(index, item) {

              /** Get item information. */
              item = $(item);
              let this_delay = index * delay_target;

              /** Trigger appear. */
              setTimeout(() => {
                $(item).addClass("appear");
                $(item).trigger("appear");

                setTimeout(() => $(item).addClass("done"), 10);
              }, this_delay);
            })
          }, delay)
        }
        return;
      }

      // TRIGGER
      // ----------------------------------------

      /** Add class when the event is triggered. */
      setTimeout(function() {
        if ((scroll + vh) > Number(attr)) el.trigger("appear").addClass("appear");

        /** Done. */
        if (done) setTimeout(() => el.addClass("appear-done"), done);
      }, delay);
    })
  }
  $(document).ready(appear());
  $('#app').on("scroll", () => appear());
} else {
  function appear() {

    /** Get main variables. */
    let vh = $(window).height();
    let scroll = window.scrollY;

    /** @setup */
    $('[appear]').each(function() {
      let el = $(this);

      /** Get element position in which the event should be triggered. */
      let on = el.attr('appear');
      if (on.indexOf('vh') != -1) {
        on = (vh / (100 / parseInt(on.replace('vh', ''))));
      } else {
        on = Number(on);
      }

      /** Get additional options. */
      let delay = (el.attr('appear-delay')) ? Number(el.attr('appear-delay')) : 0;
      let delay_target = (el.attr('appear-target-delay')) ? Number(el.attr('appear-target-delay')) : 0;
      let done = (el.attr('appear-done')) ? Number(el.attr('appear-done')) : false;

      /** Get offset from top. */
      let offset = el.offset().top;

      // TARGET (?)
      // ----------------------------------------

      /** Check for a target key. */
      let target_key = el.attr('appear-target');
      if (target_key) {
        if ((scroll + vh) > (offset + on)) {
          let targets = el.find('[appear-key="'+target_key+'"]');

          /** Loop through each target. */
          setTimeout(() => {
            targets.each(function(index, item) {

              /** Get item information. */
              item = $(item);
              let this_delay = index * delay_target;

              /** Trigger appear. */
              setTimeout(() => {
                $(item).addClass('appear');
                $(item).trigger('appear');

                setTimeout(() => $(item).addClass('done'), 10);
              }, this_delay);
            })
          }, delay)
        }
        return;
      }

      // TRIGGER
      // ----------------------------------------

      /** Add class when the event is triggered. */
      setTimeout(function() {
        if ((scroll + vh) > (offset + on)) el.trigger('appear').addClass('appear');

        /** Done. */
        if (done) setTimeout(() => el.addClass('appear-done'), done);
      }, delay);
    })
  }

  $('.neon-glitch').one('appear', function() {
    let $this = $(this)
    setTimeout(() => $this.addClass('neon-glitch-on'), 1000)
  })

  $(window).on('entered', () => {
    appear()
    $(window).on('scroll', () => appear());
  });
}
