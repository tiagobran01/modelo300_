class VintageMercadoImobiliarioSingle extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "Vintage (Mercado Imobiliário)";

    /** Main variables. */
    this.demo = {}
    this.images_preview = {}
  }

  // MAIN SETUPS
  // ----------------------------------------

  /** @setup: Slide. */
  setup__concept_slide() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('concept');

    /** Check section length. */
    if (section.length === 1) {

      /** Get main elements. */
      let slide = section.find('.slide');


      /** @inst: Slick. */
      slide.find('.slide-items').slick({
        arrows: false,
        centerMode: true,
        centerPadding: '275px',
        slidesToShow: 1,
        dots: true,
        speed: 500,
        cssEasing: 'cubic-bezier(.5, 0, 0, 1)',
        autoplay: true,
        autoplaySpeed: 7000
      });

      /** @inst: Arrows. */
      slide.find('.slide-arrows .arrow').on('click', function() {
        if ($(this).is('.arrow-next')) {
          slide.find('.slide-items').slick('slickNext');
        } else {
          slide.find('.slide-items').slick('slickPrev');
        }
      });
    }
  }

  /**
   * @setup: Videos.
   */
  setup__videos() {
    let $inst = this;

    /** Get main elements. */
    let section    = $inst.section('videos');
    let player     = section.find('.gallery .gallery-player');
    let contents   = section.find('.gallery .gallery-contents');
    let thumbnails = section.find('.gallery .gallery-thumbnails');

    /** Set the contents height. */
    let active_content_tab = contents.find('.item.active');
    let active_content_tab_height = active_content_tab.outerHeight();
    contents.height(active_content_tab_height);

    /** Bind click event to the thumbnails. */
    thumbnails.find('.item .item-link').on('click', function() {
      if ($(this).parents('.item').is('.active')) return false;

      /** Get item information. */
      let item = $(this).parents('.item');
      let item_index = item.data('index');
      let item_video = item.data('video');

      /** Change active item. */
      thumbnails.find('.item').removeClass('active');
      item.addClass('active');

      /** Change active player. */
      player.find('.link').attr('data-video', item_video);
      player.find('.images .item').removeClass('active');
      setTimeout(() => player.find('.images .item[data-index="'+item_index+'"]').addClass('active'), 500);

      /** @get: Selected content tab. */
      let selected_content = contents.find('.item[data-index="'+item_index+'"]');
      let selected_content_height = selected_content.outerHeight();

      /** Change active content. */
      contents.find('.item').removeClass('active');
      selected_content.addClass('active');
      contents.animate({
        height: selected_content_height
      },{
        duration: 1000,
        easing: $.bez([0.5, 0, 0, 1])
      })
    })

    /** Get the video modal. */
    let modal = section.find('.modal-video');

    /** Bind click event to the video player. */
    player.find('.link').on('click', function() {

      /** Get the video source. */
      let video_src = $(this).attr('data-video');

      /** Apply the video source and open the modal. */
      modal.find('iframe').attr('src', video_src);
      modal.find('iframe').on('load', () => modal.find('.video-wrapper').addClass('show'));
      modal.addClass('open');

      /** Remove the video source when the modal is closed. */
      modal.on('closed', () => modal.find('iframe').attr('src', ''));
    })
  }

  /**
   * @setup: Demo.
   *
   * @return {Promise} Resolved when finished.
   */
  setup__demo_scroll() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('demo');
    let wrapper = section.find('.section-wrapper');
    let frame   = wrapper.find('.frame');
    let content = wrapper.find('.content');

    /** Set the initial height to the section. */
    section.height(frame.height());

    /** Update content image source. */
    let image_src = content.find('img').attr('src');
    content.find('img').attr('src', '').attr('src', image_src);

    /** Continue when the image has finished loading. */
    let promise = new Promise(resolve => {
      content.find('img').on('load', function() {

        /** @save: Bleed. */
        $inst.demo.bleed = $inst.demo__calculate_bleed(frame);

        /** @save: Frame margin. */
        $inst.demo.frame_margin = (frame.height() - content.height()) - $inst.demo.bleed;

        /** Apply section height. */
        let section_height = (frame.outerHeight() + content.get(0).scrollHeight) - content.outerHeight();
        section.height(section_height + $inst.demo.frame_margin);

        /** Update saved information on resize. */
        $(window).on('resize', function() {

          /** @update: Bleed. */
          $inst.demo.bleed = $inst.demo__calculate_bleed(frame);

          /** Apply section height. */
          section_height = (frame.outerHeight() + content.get(0).scrollHeight) - content.outerHeight();
          section.height(section_height + $inst.demo.frame_margin);

          /** Update scroll position. */
          $inst.demo__update_scroll(wrapper);
        });

        /** Bind scroll event to the document. */
        $(document).on('scroll', () => $inst.demo__update_scroll(wrapper, content));
        content.scrollTop(content.get(0).scrollHeight);

        /** @resolve */
        resolve();
      })
    })

    /** @return */
    return promise;
  }

  // SECTION: DEMO
  // ----------------------------------------

  /**
   * Calculates section bleed.
   *
   * @param  {Object} frame Demo frame element.
   * @return {Number}       Bleed for vertically centering the section.
   */
  demo__calculate_bleed(frame) {
    let $inst = this;

    /** @calc */
    let vh = $(window).height();
    let fh = frame.height();
    let bleed = (vh - fh) / 2;

    /** @return */
    return bleed;
  }

  /**
   * Updates the scroll and wrapper position.
   *
   * @param {Object} wrapper Section wrapper element.
   */
  demo__update_scroll(wrapper) {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('demo');
    let frame = wrapper.find('.frame');
    let content = wrapper.find('.content');

    /** Get scroll information. */
    let scroll_position = $(window).scrollTop();
    let section_offset = (section.offset().top + 200) - $inst.demo.bleed;
    let extra_scroll = scroll_position - section_offset;

    /** Get content information. */
    let section_height = (section.offset().top + section.outerHeight()) - $(window).height();
    let is_past = (scroll_position + 203 - $inst.demo.bleed) >= section_height;

    /** Update scroll and wrapper position. */
    if ((scroll_position > section_offset) && !is_past) {

      /**
       * The current scroll position is highet than the section offset.
       * Fix the wrapper and scroll the content along with the page.
       */
      wrapper.addClass('fixed').css('top', $inst.demo.bleed);

      /** Enable the scroll for the content. */
      content.scrollTop(extra_scroll);
    } else if (!is_past) {

      /**
       * The current scroll position is lower than the section offset.
       * Reposition the wrapper and the content's scroll point.
       */
      wrapper.removeClass('fixed').css('top', 200);
      content.scrollTop(0);
    } else {

      /**
       * The current scroll position is higher than the content's scrollable area.
       * Let the section move with the page.
       */
      wrapper.removeClass('fixed').css('top', 'auto').css('bottom', 200);
    }
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {
    let $inst = this;

    /** Main setups. */
    $inst.setup__demo_scroll();
    $inst.setup__concept_slide();
    $inst.setup__videos();
  }
}
