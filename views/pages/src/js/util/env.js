
/**
 * Returns the value from a URL parameter.
 *
 * @param  {string}           name Parameter key.
 * @return {(string|boolean)}      [false] if no parameter is found.
 */
function get_url_param(name) {
  var results = new RegExp('[?&]' + name + '=([^&#]*)').exec(window.location.href);
  if (results !== null) return results[1] || 0;
  return false;
}

/**
 * Logs a message into the console.
 *
 * @param {(String|Array)} message Message to be logged.
 * @param {String} color   Prefix color.
 * @param {String} prefix  Prefix text.
 */
function log(message, color="blue", prefix="LOG") {
  if (window.dev_env === false) return false;

  /** Check for a list of messages. */
  let add = [];
  if (typeof message === 'object') {
    add = message.splice(1);
    message = message[0];
  }

  /** List pre-defined colors. */
  let colors = {
    red:    '#DC3545',
    orange: '#FF8C00',
    green:  '#28A745',
    cyan:   '#17A2B8',
    blue:   '#007BFF',
    gray:   '#6C757D',
    black:  '#343A40'
  }

  /** Get selected color. */
  let selected_color = (typeof colors[color] !== 'undefined') ? colors[color] : color;

  /** @prepare: Base prefix and message. */
  let msg_prefix = '%c'+prefix.toUpperCase();
  let msg_base = '%c'+message;

  /** @prepare: Formatting for base prefix and message. */
  let msg_prefix__formatting = 'background-color: '+selected_color+'; border: 1px solid '+selected_color+'; margin-bottom: 2px; color: white; padding: 3px 4px 2px; font-size: 10px; border-radius: 3px 0 0 3px;';
  let msg_base__formatting = 'font-weight: 600; background-color: white; border: 1px solid '+selected_color+'; border-left: none; margin-bottom: 2px; color: #343A40; padding: 3px 6px 2px; font-size: 10px; border-radius: 0 3px 3px 0;';

  /** @prepare: Additional information. */
  let msg_add_formatting = [];
  if (add.length !== 0) {

    /** @prepare: Formatting for additional information. */
    let add_formatting = 'background-color: white; border: 1px solid #BBB; margin-bottom: 2px; margin-left: 5px; color: #343A40; padding: 3px 6px 2px; font-size: 10px; border-radius: 3px;';

    /** Loop through each additional information. */
    for (let x = 0; x < add.length; x++) {
      add[x] = '%c'+add[x];
      msg_add_formatting.push(add_formatting);
    }
  }

  /** @prepare: Finished params. */
  let params_msg = [msg_prefix, msg_base, ...add].join('');
  let params_formatting = [msg_prefix__formatting, msg_base__formatting, ...msg_add_formatting];

  /** @log */
  console.log(params_msg, ...params_formatting);
}
