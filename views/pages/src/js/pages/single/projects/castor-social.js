class CastorSocialSingle extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "Castor (Social)";

    /** Main variables. */
    this.demo = {}
    this.images_preview = {}
  }

  // MAIN SETUPS
  // ----------------------------------------

  /**
   * @setup: Cards.
   */
  setup__cards() {
    let $inst = this;

    /** Bind scroll event to the document. */
    $(document).on('scroll', () => $inst.appear__cards());
    $inst.appear__cards()
  }

  /**
   * @setup: Stories.
   */
  setup__stories() {
    let $inst = this;

    /** Bind scroll event to the document. */
    $(document).on('scroll', () => $inst.appear__stories());
    $inst.appear__stories()
  }

  // APPEAR
  // ----------------------------------------

  /**
   * @appear: Cards.
   */
  appear__cards() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('cards');
    let cards = section.find('.cards');

    /** Get the current scroll position. */
    let scroll_position = $(window).scrollTop() + $(window).height();
    scroll_position = scroll_position - 200;

    /** Get the section offset. */
    let section_offset = section.offset().top;
    $(window).on('resize', () => section_offset = section.offset().top);

    /** Check if the current scroll position is higher than the section offset. */
    if (scroll_position > section_offset && !section.is('.animated')) {
      section.addClass('animated');

      /** Get cards count. */
      let cards_count = cards.find('.card').length;

      /** Animate tiles opening. */
      for (let x = 0; x < cards_count; x++) {

        /** Setup delay. */
        let delay = 150 * x;

        /** Get the current card. */
        let card = cards.find('.card[data-index="'+(x+1)+'"]');

        /** Animate the entrance for the current card. */
        setTimeout(() => card.addClass('show'), delay);
      }
    }
  }

  /**
   * @appear: Stories.
   */
  appear__stories() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('stories');
    let phones = section.find('.phones');

    /** Get the current scroll position. */
    let scroll_position = $(window).scrollTop() + $(window).height();
    scroll_position = scroll_position - 200;

    /** Get the section offset. */
    let section_offset = section.offset().top + 600;
    $(window).on('resize', () => section_offset = section.offset().top);

    /** Check if the current scroll position is higher than the section offset. */
    if (scroll_position > section_offset && !section.is('.animated')) {
      phones.addClass('animated');
    }
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {
    let $inst = this;

    /** Main setups. */
    $inst.setup__cards();
    $inst.setup__stories();
  }
}
