class ServicesPage extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "Serviços";
  }

  // CANVAS
  // ----------------------------------------

  /** Main background. */
  cv_mainBackground() {
    let $inst = this;

    /** Setup canvas for animation. */
    let bg = new services_mainBackground();
  }

  // INIT
  // ----------------------------------------

  init() {
    let $inst = this;

    /** @setup: Canvas effects. */
    $inst.cv_mainBackground();
  }
}
