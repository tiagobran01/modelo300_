class TerrazzasWebsiteSingle extends PageController {

  /** Main information for this page. */
  constructor() {
    super();
    this.pageTitle = "Terrazzas (Website)";

    /** Main variables. */
    this.demo = {}
    this.images_preview = {}
  }

  // MAIN SETUPS
  // ----------------------------------------

  /**
   * @setup: Services.
   */
  setup__services() {
    let $inst = this;

    /** Bind scroll event to the document. */
    $(document).on('scroll', () => $inst.appear__services());
    $inst.appear__services();
  }

  /**
   * @setup: Demo.
   *
   * @return {Promise} Resolved when finished.
   */
  setup__demo_scroll() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('demo');
    let wrapper = section.find('.section-wrapper');
    let frame   = wrapper.find('.frame');
    let content = wrapper.find('.content');

    /** Set the initial height to the section. */
    section.height(frame.height());

    /** Update content image source. */
    let image_src = content.find('img').attr('src');
    content.find('img').attr('src', '').attr('src', image_src);

    /** Continue when the image has finished loading. */
    let promise = new Promise(resolve => {
      content.find('img').on('load', function() {

        /** @save: Bleed. */
        $inst.demo.bleed = $inst.demo__calculate_bleed(frame);

        /** @save: Frame margin. */
        $inst.demo.frame_margin = (frame.height() - content.height()) - $inst.demo.bleed;

        /** Apply section height. */
        let section_height = (frame.outerHeight() + content.get(0).scrollHeight) - content.outerHeight();
        section.height(section_height + $inst.demo.frame_margin);

        /** Update saved information on resize. */
        $(window).on('resize', function() {

          /** @update: Bleed. */
          $inst.demo.bleed = $inst.demo__calculate_bleed(frame);

          /** Apply section height. */
          section_height = (frame.outerHeight() + content.get(0).scrollHeight) - content.outerHeight();
          section.height(section_height + $inst.demo.frame_margin);

          /** Update scroll position. */
          $inst.demo__update_scroll(wrapper);
        });

        /** Bind scroll event to the document. */
        $(document).on('scroll', () => $inst.demo__update_scroll(wrapper, content));
        content.scrollTop(content.get(0).scrollHeight);

        /** @resolve */
        resolve();
      })
    })

    /** @return */
    return promise;
  }

  /**
   * @setup: Cards.
   */
  setup__cards() {
    let $inst = this;

    /** Bind scroll event to the document. */
    $(document).on('scroll', () => $inst.appear__cards());
    $inst.appear__cards()
  }

  /**
   * @setup: Videos.
   */
  setup__videos() {
    let $inst = this;

    /** Get main elements. */
    let section    = $inst.section('videos');
    let player     = section.find('.gallery .gallery-player');
    let contents   = section.find('.gallery .gallery-contents');
    let thumbnails = section.find('.gallery .gallery-thumbnails');

    /** Set the contents height. */
    let active_content_tab = contents.find('.item.active');
    let active_content_tab_height = active_content_tab.outerHeight();
    contents.height(active_content_tab_height);

    /** Bind click event to the thumbnails. */
    thumbnails.find('.item .item-link').on('click', function() {
      if ($(this).parents('.item').is('.active')) return false;

      /** Get item information. */
      let item = $(this).parents('.item');
      let item_index = item.data('index');
      let item_video = item.data('video');

      /** Change active item. */
      thumbnails.find('.item').removeClass('active');
      item.addClass('active');

      /** Change active player. */
      player.find('.link').attr('data-video', item_video);
      player.find('.images .item').removeClass('active');
      setTimeout(() => player.find('.images .item[data-index="'+item_index+'"]').addClass('active'), 500);

      /** @get: Selected content tab. */
      let selected_content = contents.find('.item[data-index="'+item_index+'"]');
      let selected_content_height = selected_content.outerHeight();

      /** Change active content. */
      contents.find('.item').removeClass('active');
      selected_content.addClass('active');
      contents.animate({
        height: selected_content_height
      },{
        duration: 1000,
        easing: $.bez([0.5, 0, 0, 1])
      })
    })

    /** Get the video modal. */
    let modal = section.find('.modal-video');

    /** Bind click event to the video player. */
    player.find('.link').on('click', function() {

      /** Get the video source. */
      let video_src = $(this).attr('data-video');

      /** Apply the video source and open the modal. */
      modal.find('iframe').attr('src', video_src);
      modal.find('iframe').on('load', () => modal.find('.video-wrapper').addClass('show'));
      modal.addClass('open');

      /** Remove the video source when the modal is closed. */
      modal.on('closed', () => modal.find('iframe').attr('src', ''));
    })
  }

  /**
   * @setup: Images preview.
   */
  setup__images_preview() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('images');
    let slider = section.find('.preview .preview-items');
    let slide_count = slider.find('.item').length;

    /** Set interval for automatic slide change. */
    $inst.images_preview.interval = setInterval(() => {

      /** Get the current slide. */
      let current_slide = slider.find('.item.active');
      let current_slide_index = current_slide.data('index');

      /** Get the next slide. */
      let next_slide_index = (current_slide_index + 1 <= slide_count) ? current_slide_index + 1 : 1;
      let next_slide = slider.find('.item[data-index="'+next_slide_index+'"]');

      /** Change active slide. */
      current_slide.removeClass('active');
      setTimeout(() => next_slide.addClass('active'), 500);
    }, 5000);
  }

  /**
   * @setup: Images scroll.
   *
   * @return {Promise} Resolved when finished.
   */
  setup__images_scroll() {
    let $inst = this;

    /** Get main elements. */
    let section     = $inst.section('images');
    let frames      = section.find('.flex-wrapper .left, .flex-wrapper .right');
    let frame_left  = section.find('.flex-wrapper .left');
    let frame_right = section.find('.flex-wrapper .right');

    /** Update image source. */
    let image_src = section.find('.page-image img').attr('src');
    section.find('.page-image img').attr('src', '').attr('src', image_src);

    /** Continue when the image has loaded. */
    let promise = new Promise(resolve => {
      section.find('.page-image img').on('load', function() {

        /** Get initial main information. */
        let vh             = $(window).height();
        let section_offset = section.offset().top;
        let section_height = section.height();
        let section_end    = section_offset + section.height();
        let margin_left    = section.find('.section-container').offset().left;
        let margin_right   = $(window).width() - (margin_left + section.find('.section-container').width());

        /** Setup data to use as a base for the calculations. */
        let data = {
          vh: vh,
          section_offset: section_offset,
          section_height: section_height,
          section_end: section_end,
          margin_left: margin_left,
          margin_right: margin_right
        };

        /** Setup elements list. */
        let elements = {
          frames: frames,
          frame_left: frame_left,
          frame_right: frame_right
        }

        /** Update main information. */
        $(window).on('resize', () => {

          /** Update variables. */
          vh             = $(window).height();
          section_offset = section.offset().top;
          section_height = section.height();
          section_end    = section_offset + section.height();
          margin_left    = section.find('.section-container').offset().left;
          margin_right   = $(window).width() - (margin_left + section.find('.section-container').width());

          /** Update data. */
          data = {
            vh: vh,
            section_offset: section_offset,
            section_height: section_height,
            section_end: section_end,
            margin_left: margin_left,
            margin_right: margin_right
          };

          /** Update scroll. */
          $inst.images__update_scroll(data, elements);
        })

        /** Update scroll. */
        $inst.images__update_scroll(data, elements);

        /** Bind scroll event to the document. */
        $(document).on('scroll', () => $inst.images__update_scroll(data, elements));

        /** @resolve */
        resolve();
      })
    })

    /** @return */
    return promise;
  }

  /**
   * @setup: Tiles.
   */
  setup__tiles() {
    let $inst = this;

    /** Bind scroll event to the document. */
    $(document).on('scroll', () => $inst.appear__tiles());
    $inst.appear__tiles();
  }

  // SECTION: DEMO
  // ----------------------------------------

  /**
   * Calculates section bleed.
   *
   * @param  {Object} frame Demo frame element.
   * @return {Number}       Bleed for vertically centering the section.
   */
  demo__calculate_bleed(frame) {
    let $inst = this;

    /** @calc */
    let vh = $(window).height();
    let fh = frame.height();
    let bleed = (vh - fh) / 2;

    /** @return */
    return bleed;
  }

  /**
   * Updates the scroll and wrapper position.
   *
   * @param {Object} wrapper Section wrapper element.
   */
  demo__update_scroll(wrapper) {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('demo');
    let frame = wrapper.find('.frame');
    let content = wrapper.find('.content');

    /** Get scroll information. */
    let scroll_position = $(window).scrollTop();
    let section_offset = (section.offset().top + 200) - $inst.demo.bleed;
    let extra_scroll = scroll_position - section_offset;

    /** Get content information. */
    let section_height = (section.offset().top + section.outerHeight()) - $(window).height();
    let is_past = (scroll_position + 203 - $inst.demo.bleed) >= section_height;

    /** Update scroll and wrapper position. */
    if ((scroll_position > section_offset) && !is_past) {

      /**
       * The current scroll position is highet than the section offset.
       * Fix the wrapper and scroll the content along with the page.
       */
      wrapper.addClass('fixed').css('top', $inst.demo.bleed);

      /** Enable the scroll for the content. */
      content.scrollTop(extra_scroll);
    } else if (!is_past) {

      /**
       * The current scroll position is lower than the section offset.
       * Reposition the wrapper and the content's scroll point.
       */
      wrapper.removeClass('fixed').css('top', 200);
      content.scrollTop(0);
    } else {

      /**
       * The current scroll position is higher than the content's scrollable area.
       * Let the section move with the page.
       */
      wrapper.removeClass('fixed').css('top', 'auto').css('bottom', 200);
    }
  }

  // SECTION: IMAGES
  // ----------------------------------------

  /**
   * Updates the scroll and wrapper position.
   *
   * @param {Object} data     Data to use as a base for the decisions.
   * @param {Object} elements Elements to perform modifications on.
   */
  images__update_scroll(data, elements) {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('images');

    /** @separate: Main information. */
    let {
      vh,
      section_offset,
      section_height,
      section_end,
      margin_left,
      margin_right
    } = data;

    /** @separate: Elements. */
    let {
      frames,
      frame_left,
      frame_right
    } = elements;

    /** Get current scroll position.. */
    let scroll_position = $(window).scrollTop();

    /** Get other elements. */
    let line = section.find('.flex-wrapper .right .scroll-bar .fill');

    /** Fix the frames if the current scroll point is higher than the section offset. */
    if (scroll_position < section_offset) {

      /** {Before} */
      frames.removeClass('fix passed');
      frame_left.css('left', 0);
      frame_right.css('right', 0);
    } else if (scroll_position > section_offset && (scroll_position + vh) < section_end) {

      /** {Within} */
      frames.removeClass('passed').addClass('fix');
      frame_left.css('left', margin_left);
      frame_right.css('right', margin_right);
    } else {

      /** {After} */
      frames.removeClass('fix').addClass('passed');
      frame_left.css('left', 0);
      frame_right.css('right', 0);
    }

    /** Fill the scroll bar as the user scrolls past the section. */
    if ((scroll_position + vh) > section_offset && (scroll_position + vh) <= section_end) {

      /** Calculate the scroll percentage within the wrapper. */
      let scroll_percentage = (scroll_position + vh) - section_offset;
      scroll_percentage = (scroll_percentage / section_height) * 100;
      scroll_percentage = scroll_percentage / 100;

      /** Fill the line based on the current scroll position. */
      line.css('transform', 'scaleY('+scroll_percentage+')');
    }
  }

  // APPEAR
  // ----------------------------------------

  /**
   * @appear: Services.
   */
  appear__services() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('services').find('.section-blocks');
    let blocks = section.find('.blocks');
    let line = section.find('.line');

    /** Get the current scroll position. */
    let scroll_position = $(window).scrollTop() + $(window).height();
    scroll_position = scroll_position - 200;

    /** Get the section offset. */
    let section_offset = section.offset().top;
    $(window).on('resize', () => section_offset = section.offset().top);

    /** Check if the current scroll position is higher than the section offset. */
    if (scroll_position > section_offset && !section.is('.animated')) {
      section.addClass('animated');

      /** Calculate the delay between each block. */
      let blocks_count = blocks.find('.block').length;
      let delay_between = Number((2000 / blocks_count).toFixed(2));

      /** Animate the line. */
      line.addClass('show');

      /** Animate the blocks. */
      for (let x = 0; x < blocks_count; x++) {

        /** Calculate the delay. */
        let delay = delay_between * x;

        /** Animate the block. */
        setTimeout(() => blocks.find('.block[data-index="'+x+'"]').addClass('show'), delay);
      }
    }
  }

  /**
   * @appear: Cards.
   */
  appear__cards() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('cards');
    let cards = section.find('.cards');

    /** Get the current scroll position. */
    let scroll_position = $(window).scrollTop() + $(window).height();
    scroll_position = scroll_position - 200;

    /** Get the section offset. */
    let section_offset = section.offset().top;
    $(window).on('resize', () => section_offset = section.offset().top);

    /** Check if the current scroll position is higher than the section offset. */
    if (scroll_position > section_offset && !section.is('.animated')) {
      section.addClass('animated');

      /** Get cards count. */
      let cards_count = cards.find('.card').length;

      /** Animate tiles opening. */
      for (let x = 0; x < cards_count; x++) {

        /** Setup delay. */
        let delay = 150 * x;

        /** Get the current card. */
        let card = cards.find('.card[data-index="'+(x+1)+'"]');

        /** Animate the entrance for the current card. */
        setTimeout(() => card.addClass('show'), delay);
      }
    }
  }

  /**
   * @appear: Tiles.
   */
  appear__tiles() {
    let $inst = this;

    /** Get main elements. */
    let section = $inst.section('tiles');

    /** Get the current scroll position. */
    let scroll_position = $(window).scrollTop() + $(window).height();
    scroll_position = scroll_position - 200;

    /** Get the section offset. */
    let section_offset = section.offset().top;
    $(window).on('resize', () => section_offset = section.offset().top);

    /** Check if the current scroll position is higher than the section offset. */
    if (scroll_position > section_offset && !section.is('.animated')) {
      section.addClass('animated');

      /** Animate tiles opening. */
      for (let x = 0; x < 20; x++) {
        if (section.find('.tile-part[data-index="'+x+'"]').length !== 0) {
          let tile_part = section.find('.tile-part[data-index="'+x+'"]');

          /** Setup delay. */
          let delay = 1000 * (x - 1);

          /** Animate part. */
          setTimeout(() => tile_part.addClass('active'), delay);
        }
      }
    }
  }

  // INIT
  // ----------------------------------------

  /** @init */
  init() {
    let $inst = this;

    /** Main setups. */
    $inst.setup__videos();
    $inst.setup__images_preview();
    Promise.all([
      $inst.setup__demo_scroll(),
      $inst.setup__images_scroll()
    ]).then(() => {
      $inst.setup__services();
      $inst.setup__tiles();
      $inst.setup__cards();
    })
  }
}
