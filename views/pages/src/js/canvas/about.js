
// HERO
// ----------------------------------------

class about_heroBackground {

  constructor(container) {
    const { PI, cos, sin, abs, sqrt, pow, round, random, atan2 } = Math;
    const HALF_PI = 0.5 * PI;
    const TAU = 2 * PI;
    const TO_RAD = PI / 180;
    const floor = n => n | 0;
    const rand = n => n * random();
    const randIn = (min, max) => rand(max - min) + min;
    const randRange = n => n - rand(2 * n);
    const fadeIn = (t, m) => t / m;
    const fadeOut = (t, m) => (m - t) / m;
    const fadeInOut = (t, m) => {
      let hm = 0.5 * m;
      return abs((t + hm) % m - hm) / (hm);
    };
    const dist = (x1, y1, x2, y2) => sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
    const angle = (x1, y1, x2, y2) => atan2(y2 - y1, x2 - x1);
    const lerp = (n1, n2, speed) => (1 - speed) * n1 + speed * n2;

    const particleCount = 700;
    const particlePropCount = 9;
    const particlePropsLength = particleCount * particlePropCount;
    const baseTTL = 100;
    const rangeTTL = 500;
    const baseSpeed = 0.1;
    const rangeSpeed = 1;
    const baseSize = 2;
    const rangeSize = 10;
    const baseHue = 10;
    const rangeHue = 100;
    const noiseSteps = 2;
    const xOff = 0.0025;
    const yOff = 0.005;
    const zOff = 0.0005;
    const backgroundColor = '#151515';

    let canvas;
    let ctx;
    let center;
    let gradient;
    let tick;
    let particleProps;
    let positions;
    let velocities;
    let lifeSpans;
    let speeds;
    let sizes;
    let hues;

    function setup() {
      createCanvas();
      resize();
      initParticles();
      draw();
    }

    function initParticles() {
      tick = 0;
      particleProps = new Float32Array(particlePropsLength);

      let i;

      for (i = 0; i < particlePropsLength; i += particlePropCount) {
        initParticle(i);
      }
    }

    function initParticle(i) {
      let theta, x, y, vx, vy, life, ttl, speed, size, hue;

      x = rand(canvas.a.width);
      y = rand(canvas.a.height);
      theta = angle(x, y, center[0], center[1]);
      vx = cos(theta) * 6;
      vy = sin(theta) * 6;
      life = 0;
      ttl = baseTTL + rand(rangeTTL);
      speed = baseSpeed + rand(rangeSpeed);
      size = baseSize + rand(rangeSize);
      hue = baseHue + rand(rangeHue);

      particleProps.set([x, y, vx, vy, life, ttl, speed, size, hue], i);
    }

    function drawParticles() {
      let i;

      for (i = 0; i < particlePropsLength; i += particlePropCount) {
        updateParticle(i);
      }
    }

    function updateParticle(i) {
      let i2=1+i, i3=2+i, i4=3+i, i5=4+i, i6=5+i, i7=6+i, i8=7+i, i9=8+i;
      let x, y, theta, vx, vy, life, ttl, speed, x2, y2, size, hue;

      x = particleProps[i];
      y = particleProps[i2];
      theta = angle(x, y, center[0], center[1]) + 0.75 * HALF_PI;
      vx = lerp(particleProps[i3], 2 * cos(theta), 0.05);
      vy = lerp(particleProps[i4], 2 * sin(theta), 0.05);
      life = particleProps[i5];
      ttl = particleProps[i6];
      speed = particleProps[i7];
      x2 = x + vx * speed;
      y2 = y + vy * speed;
      size = particleProps[i8];
      hue = particleProps[i9];

      drawParticle(x, y, theta, life, ttl, size, hue);

      life++;

      particleProps[i] = x2;
      particleProps[i2] = y2;
      particleProps[i3] = vx;
      particleProps[i4] = vy;
      particleProps[i5] = life;

      life > ttl && initParticle(i);
    }

    function drawParticle(x, y, theta, life, ttl, size, hue) {
      let xRel = x - (0.5 * size), yRel = y - (0.5 * size);

      ctx.a.save();
      ctx.a.lineCap = 'round';
      ctx.a.lineWidth = 1;
      ctx.a.strokeStyle = `hsla(${hue},100%,60%,${fadeInOut(life, ttl)})`;
      ctx.a.beginPath();
      ctx.a.translate(xRel, yRel);
      ctx.a.rotate(theta);
      ctx.a.translate(-xRel, -yRel);
      ctx.a.strokeRect(xRel, yRel, size, size);
      ctx.a.closePath();
      ctx.a.restore();
    }

    function createCanvas() {
      canvas = {
        a: document.createElement('canvas'),
        b: document.createElement('canvas')
      };
      canvas.b.style = `
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      `;
      container.appendChild(canvas.b);
      ctx = {
        a: canvas.a.getContext('2d'),
        b: canvas.b.getContext('2d')
      };
      center = [];
    }

    function resize() {
      const { innerWidth, innerHeight } = window;

      canvas.a.width = innerWidth;
      canvas.a.height = innerHeight;

      ctx.a.drawImage(canvas.b, 0, 0);

      canvas.b.width = innerWidth;
      canvas.b.height = innerHeight;

      ctx.b.drawImage(canvas.a, 0, 0);

      center[0] = 0.5 * canvas.a.width;
      center[1] = 0.5 * canvas.a.height;
    }

    function renderGlow() {
      ctx.b.save();
      ctx.b.filter = 'blur(8px) brightness(200%)';
      ctx.b.globalCompositeOperation = 'lighter';
      ctx.b.drawImage(canvas.a, 0, 0);
      ctx.b.restore();

      ctx.b.save();
      ctx.b.filter = 'blur(4px) brightness(200%)';
      ctx.b.globalCompositeOperation = 'lighter';
      ctx.b.drawImage(canvas.a, 0, 0);
      ctx.b.restore();
    }

    function render() {
      ctx.b.save();
      ctx.b.globalCompositeOperation = 'lighter';
      ctx.b.drawImage(canvas.a, 0, 0);
      ctx.b.restore();
    }

    function draw() {
      tick++;

      ctx.a.clearRect(0, 0, canvas.a.width, canvas.a.height);

      ctx.b.fillStyle = backgroundColor;
      ctx.b.fillRect(0, 0, canvas.a.width, canvas.a.height);

      drawParticles();
      renderGlow();
      render();

      window.requestAnimationFrame(draw);
    }

    window.addEventListener('load', setup);
    window.addEventListener('resize', resize);
  }
}

// CASES
// ----------------------------------------

class about_casesBackground {

  /**
   * Constructor.
   */
  constructor(options) {
    this.$el = options.el;
    this.time = 0;
    this.bindAll();
    this.init();
  }

  /**
   * Bind particles.
   */
  bindAll() {
    this.render = this.render.bind(this);
    this.resize = this.resize.bind(this);
  }

  /**
   * Init.
   */
  init() {
    this.textureLoader = new THREE.TextureLoader();
    this.camera = new THREE.PerspectiveCamera( 20, window.innerWidth / window.innerHeight, 1, 2000 );
    this.camera.position.z = 350;
    this.camera.position.y = 200;
    this.camera.lookAt(new THREE.Vector3(0, 0, 20));

    this.scene = new THREE.Scene();

    this.renderer = new THREE.WebGLRenderer({ alpha: true });
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize( window.innerWidth, window.innerHeight );
    this.$el.appendChild( this.renderer.domElement );


    this.createParticles();
    this.bindEvents();
    this.resize();
    this.render();
  }

  /**
   * Generates particles.
   */
  createParticles() {
    const plane = new THREE.PlaneBufferGeometry(500, 250, 250, 125);

    const textureLoader = new THREE.TextureLoader();
    textureLoader.crossOrigin = '';

    let texture_path = $(this.$el).attr('data-spark');

    const material = new THREE.ShaderMaterial({
      uniforms: {
        time: {
          value: 1.0
        },
        texture: {
          value: textureLoader.load(texture_path)
        },
        resolution: {
          value: new THREE.Vector2()
        }
      },

      vertexShader: document.getElementById('sobrePage-vertex-shader').textContent,
      fragmentShader: document.getElementById('sobrePage-fragment-shader').textContent,
      blending: THREE.AdditiveBlending,
      depthTest: false,
      transparent: true
    });

    this.particles = new THREE.Points(plane, material);
    this.particles.rotation.x = this.degToRad(-90);

    this.scene.add(this.particles);
  }

  /**
   * Bind events.
   */
  bindEvents() {
    window.addEventListener('resize', this.resize);
  }

  /**
   * Update canvas dimensions.
   */
  resize() {
    const w = window.innerWidth;
    const h = window.innerHeight;
    this.renderer.setSize(w,h);
    this.camera.aspect = w/h;
    this.camera.updateProjectionMatrix();
  }

  /**
   * Moves particles.
   */
  moveParticles() {
    this.particles.material.uniforms.time.value = this.time;
  }

  /**
   * Renders animation.
   */
  render() {
    requestAnimationFrame(this.render);
    this.time += .01;

    this.moveParticles();
    this.renderer.render(this.scene, this.camera);
  }

  /**
   * Utility.
   */
  degToRad(angle) {
    return angle * Math.PI / 180;
  }
}

// FOOTER
// ----------------------------------------

class about_footerParticle {
  constructor(x, y, config, ctx) {
    this.pos = new Vector(x, y);
    this.prevPos = new Vector(x, y);
    this.vel = new Vector(Math.random() - 0.5, Math.random() - 0.5);
    this.acc = new Vector(0, 0);
    this.config = config;
    this.ctx = ctx;
  }

  move(acc) {
    this.prevPos.x = this.pos.x;
    this.prevPos.y = this.pos.y;
    if(acc) {
      this.acc.addTo(acc);
    }
    this.vel.addTo(this.acc);
    this.pos.addTo(this.vel);
    if(this.vel.getLength() > this.config.particleSpeed) {
      this.vel.setLength(this.config.particleSpeed);
    }
    this.acc.x = 0;
    this.acc.y = 0;
  }

  drawLine() {
    this.ctx.beginPath();
    this.ctx.moveTo(this.prevPos.x, this.prevPos.y);
    this.ctx.lineTo(this.pos.x, this.pos.y);
    this.ctx.stroke();
  }

  wrap() {
    if(this.pos.x > this.w) {
      this.prevPos.x = this.pos.x = 0;
    } else if(this.pos.x < 0) {
      this.prevPos.x = this.pos.x = this.w - 1;
    }
    if(this.pos.y > this.h) {
      this.prevPos.y = this.pos.y = 0;
    } else if(this.pos.y < 0) {
      this.prevPos.y = this.pos.y = this.h - 1;
    }
  }
}

class about_footerBackground {

  setup() {
    let $inst = this;

    $inst.size = 3;
    $inst.noiseZ = 0;
    $inst.ctx = $inst.canvas.getContext("2d");
    window.addEventListener("resize", $inst.reset);

    $inst.config = {
      zoom: 80,
      noiseSpeed: 0.007,
      particleSpeed: 1,
      fieldForce: 20,
    };

    $inst.colorConfig = {
      particleOpacity: 0.3,
      baseHue: 0,
      hueRange: 350,
      hueSpeed: 0.0005,
      colorSaturation: 100,
    };
    $inst.reset();
  }

  reset() {
    let $inst = this;

    $inst.hue = $inst.colorConfig.baseHue;
    noise.seed(Math.random());
    $inst.w = $inst.canvas.width = window.innerWidth;
    $inst.h = $inst.canvas.height = 300;
    $inst.columns = Math.floor($inst.w / $inst.size) + 1;
    $inst.rows = Math.floor($inst.h / $inst.size) + 1;
    $inst.initParticles();
    $inst.initField();
    $inst.drawText();
    $inst.drawBackground(1);
  }

  initParticles() {
    let $inst = this;

    $inst.particles = [];
    let numberOfParticles = $inst.w * $inst.h / 300;
    for (let i = 0; i < numberOfParticles; i++) {
      let particle = new about_footerParticle(Math.random() * $inst.w, Math.random() * $inst.h, $inst.config, $inst.ctx);
      $inst.particles.push(particle);
    }
  }

  draw() {
    let $inst = this;
    $inst.calculateField();
    $inst.noiseZ += $inst.config.noiseSpeed;
    $inst.drawParticles();
  }

  initField() {
    let $inst = this;

    $inst.field = new Array($inst.columns);
    for (let x = 0; x < $inst.columns; x++) {
      $inst.field[x] = new Array($inst.columns);
      for (let y = 0; y < $inst.rows; y++) {
        $inst.field[x][y] = new Vector(0, 0);
      }
    }
  }

  calculateField() {
    let $inst = this;

    let x1;
    let y1;
    for (let x = 0; x < $inst.columns; x++) {
      for (let y = 0; y < $inst.rows; y++) {
        let color = $inst.buffer32[y*$inst.size * $inst.w + x*$inst.size];
        if (color) {
          x1 = (Math.random()-0.5) * 3;
          y1 = (Math.random()-0.5) * 3;
        } else {
          x1 = noise.simplex3(x/$inst.config.zoom, y/$inst.config.zoom, $inst.noiseZ) * $inst.config.fieldForce / 20;
          y1 = noise.simplex3(x/$inst.config.zoom + 40000, y/$inst.config.zoom + 40000, $inst.noiseZ) * $inst.config.fieldForce / 20;

        }
        $inst.field[x][y].x = x1;
        $inst.field[x][y].y = y1;
      }
    }
  }

  drawBackground(alpha) {
    let $inst = this;

    $inst.ctx.fillStyle = `#151515`;
    $inst.ctx.fillRect(0, 0, $inst.w, $inst.h);
  }

  drawText() {
    let $inst = this;

    $inst.ctx.save();
    let text = "300";
    let fontSize = 400;
    $inst.ctx.font = "bold " + fontSize + "px sans-serif";
    var textWidth = $inst.ctx.measureText(text).width;
    var marginLeft = ($inst.w - textWidth) * 0.5;
    var marginTop = ($inst.h - fontSize) * 0.69;

    $inst.ctx.fillStyle = "white";
    $inst.ctx.fillText(text, marginLeft, marginTop + fontSize*0.9);
    $inst.ctx.restore();
    let image = $inst.ctx.getImageData(0, 0, $inst.w, $inst.h);
    $inst.buffer32 = new Uint32Array(image.data.buffer);
  }

  drawParticles() {
    let $inst = this;

    $inst.hue += $inst.colorConfig.hueSpeed;
    let h = Math.sin($inst.hue) * $inst.colorConfig.hueRange + $inst.colorConfig.baseHue;
    $inst.ctx.strokeStyle = `hsla(${h}, ${$inst.colorConfig.colorSaturation}%, 50%, ${$inst.colorConfig.particleOpacity})`;
    let x;
    let y;
    $inst.particles.forEach(p => {
      x = p.pos.x / $inst.size;
      y = p.pos.y / $inst.size;
      let v;
      if (x >= 0 && x < $inst.columns && y >= 0 && y < $inst.rows) {
        v = $inst.field[Math.floor(x)][Math.floor(y)];
      }
      p.move(v);
      p.wrap();
      p.drawLine();
    });
  }
}
